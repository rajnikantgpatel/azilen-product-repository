<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="panel panel-primary" >
	<div class="panel-heading">
		<div class="panel-title"><h1>Welcome to Azilen Product application</h1></div>
	</div>
	<div style="padding-top:30px" class="panel-body" >
		<c:choose>
			<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
			</c:when>
			<c:when test='${successMessage != null && !successMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
			</c:when>
			<c:otherwise>
				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
			</c:otherwise>
		</c:choose>
		<div>
			<c:if test='${loggedInUser != null}'>
			<div class="panel">
				<div class="panel-body">
					<c:url value="/searchCompanyOrProduct" var="searchUrl" />
					<form:form class="navbar-form navbar-right"  action="${searchUrl}" role="search" commandName="searchKeyword" modelAttribute="searchKeyword" method="post" data-toggle="validator">
						<div class="form-group">
							<form:input type="text" path="searchCompanyOrProductKeywords" class="form-control" placeholder="Enter keywords for product or company" required="required" />
						</div>
						<form:button type="submit" class="btn btn-default">Search product</form:button>
					</form:form>
				</div>
			</div>
			<c:if test="${searchKeyword != null && searchKeyword.getSearchCompanyOrProductKeywords() != null && !searchKeyword.getSearchCompanyOrProductKeywords().equals('') }">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<div class="panel-title"><h4>Products</h4></div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Product Name</th>
									<th>Product code</th>
									<th>More details</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listProducts}" var="product">
									<tr>
										<td>${product.id}</td>
										<td>${product.productName}</td>
										<td>${product.productCode}</td>
										<td><a href="<c:url value="/product/show/${product.id}" />">Show details</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="panel-title"><h4>Companies</h4></div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Company Name</th>
									<th>Logo</th>
									<th>Total employees</th>
									<th>More details</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listCompanies}" var="company">
									<tr>
										<td>${company.id}</td>
										<td>${company.companyName}</td>
										<td>${company.logo}</td>
										<td>${company.employeeStrength}</td>
										<td><a href="<c:url value="/company/show/${company.id}" />">Show details</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</c:if>
			</c:if>
		</div>
	</div>
</div>