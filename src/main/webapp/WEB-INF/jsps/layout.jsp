<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="<c:url value="/resources/images/images.png" />">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Azilen Product - <tiles:insertAttribute name="title" ignore="true" /></title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">


<!-- Optional theme -->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-theme.min.css" />">
</head>
<body>

	<div>
		<tiles:insertAttribute name="header" />
	</div>
	<!-- Latest compiled and minified JavaScript -->
	<script type="text/javascript" src='<c:url value="/resources/js/jquery-2.1.4.min.js" />'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/validator.js" />'></script>
	<script type="text/javascript" src='<c:url value="/resources/js/bootstrap.min.js" />'></script>
	<div class="container">
		<div
			style="float: left; padding: 10px; width: 80%;">
			<tiles:insertAttribute name="body" />
		</div>
		<div style="clear: both">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>
