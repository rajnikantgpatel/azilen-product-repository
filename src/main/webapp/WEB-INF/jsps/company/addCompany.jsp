<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="panel panel-primary">
	<div class="panel-heading">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a style="color:#ffffff;" href="<c:url value="/company/companies" />">Companies</a>
			</div>
		</div>
		<div class="panel-title"><h1>${action} company</h1>
	</div>
	</div>
	<div class="panel-body">
		<c:choose>
			<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
			</c:when>
			<c:when test='${successMessage != null && !successMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
			</c:when>
			<c:otherwise>
				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
			</c:otherwise>
		</c:choose>
		<c:url var="addUrl" value="/company/save"/>
		<form:form id="addCompanyForm" action="${addUrl}" method="post" data-toggle="validator" role="form"
			data-fv-framework="bootstrap" data-fv-icon-valid="glyphicon glyphicon-ok" data-fv-icon-invalid="glyphicon glyphicon-remove"
		    data-fv-icon-validating="glyphicon glyphicon-refresh" commandName="company" modelAttribute="company" enctype="multipart/form-data">
			<div class="form-group">
				<form:input type="hidden" id="id" path="id" />
			</div>
			<div class="form-group">
				<label for="companyName" class="col-md-3 control-label">Company name *</label>
				<div class="col-md-9">
					<form:input type="text" maxlength="255" class="form-control" id="companyName" path="companyName" placeholder="Company name" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="address"  class="col-md-3 control-label">Company address *</label>
				<div class="col-md-9">
					<form:textarea rows="5" maxlength="1000" cols="50" class="form-control" id="address" path="address" placeholder="Company address" required="required" ></form:textarea>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="logo"  class="col-md-3 control-label">Company logo *</label>
				<div class="col-md-9">
					<form:input type="file" id="file" class="form-control" placeholder="Company logo" required="required" maxlength="100" path="file" />
					<%-- <form:input type="text" id="logo" path="logo"  /> --%>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="contactEmailAddress" class="col-md-3 control-label">Contact email address *</label>
				<div class="col-md-9">
					<form:input type="email" class="form-control" maxlength="100" id="contactEmailAddress" path="contactEmailAddress" placeholder="Contact Email Address"  data-error="Hey, please enter valid email address for contact email address" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
		        <label for="countrySelectBox" class="col-md-3 control-label">Country</label>
				<div class="col-md-9">
		            <select class="form-control" name="countrySelectBox">
		                <option value="US">United States</option>
		                <option value="BG">Bulgaria</option>
		                <option value="BR">Brazil</option>
		                <option value="CN">China</option>
		                <option value="CZ">Czech Republic</option>
		                <option value="DK">Denmark</option>
		                <option value="FR">France</option>
		                <option value="DE">Germany</option>
		                <option value="IN">India</option>
		                <option value="MA">Morocco</option>
		                <option value="NL">Netherlands</option>
		                <option value="PK">Pakistan</option>
		                <option value="RO">Romania</option>
		                <option value="RU">Russia</option>
		                <option value="SK">Slovakia</option>
		                <option value="ES">Spain</option>
		                <option value="TH">Thailand</option>
		                <option value="AE">United Arab Emirates</option>
		                <option value="GB">United Kingdom</option>
		                <option value="VE">Venezuela</option>
		            </select>
		            <div class="help-block with-errors"></div>
	            </div>
		    </div>
			<div class="form-group">
				<label for="contactLandlineNumber"  class="col-md-3 control-label">Contact Landline Number *</label>
				<div class="col-md-9">
					<form:input type="text" class="form-control" id="contactLandlineNumber" path="contactLandlineNumber" placeholder="Contact landline number" maxlength="15" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="employeeStrength"  class="col-md-3 control-label">Total employees *</label>
				<div class="col-md-9">
					<form:input type="number" class="form-control" maxlength="5" id="employeeStrength" path="employeeStrength" placeholder="Employee Strength" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<form:button type="submit" id="btnSave" class="btn btn-success">Save</form:button>
    			<form:button type="button" id="btnCancel" class="btn btn-danger">Cancel</form:button>
  			</div>
		</form:form>
	</div>
	<div class="panel-footer">
    				
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btnCancel").click(function(){
		        document.location = '<c:url value="/company/companies" />';
		    });
		    $('#addCompanyForm')
		        .formValidation()
		        // Revalidate phone number when changing the country
		        .on('change', '[name="countrySelectBox"]', function(e) {
		            $('#addCompanyForm').formValidation('revalidateField', 'contactLandlineNumber');
	        });
		});
	</script>
</div>