<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-primary">
	<div class="panel-heading">Companies</div>
	<div class="panel-body">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a href="<c:url value="/company/add" />">Add new company</a>
			</div>
		</div>
		<h2>List of companies</h2>
	</div>
	<c:choose>
		<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
			<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
		</c:when>
		<c:when test='${successMessage != null && !successMessage.equals("")}'>
			<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
		</c:when>
		<c:otherwise>
			<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
		</c:otherwise>
	</c:choose>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Company Name</th>
					<th>Logo</th>
					<th>Total employees</th>
					<th>More details</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listCompanies}" var="company">
					<tr>
						<td>${company.id}</td>
						<td>${company.companyName}</td>
						<td><img  src="<c:url value="/resources/company/${company.id}.jpg" />" height="30px" width="100px" alt="..."></td>
						<td>${company.employeeStrength}</td>
						<td><a href="<c:url value="/company/show/${company.id}" />">Show details</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>