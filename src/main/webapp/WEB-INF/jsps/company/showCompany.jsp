<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="panel panel-primary">
	<div class="panel-heading">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a style="color:#ffffff;" href="<c:url value="/company/companies" />">Companies</a>
			</div>
		</div>
		<div class="panel-title"><h1>Details of ${company.companyName}</h1></div>
	</div>
	<div class="panel-body">
		<c:choose>
			<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
			</c:when>
			<c:when test='${successMessage != null && !successMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
			</c:when>
			<c:otherwise>
				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
			</c:otherwise>
		</c:choose>
		<div class="form-group">
			<input type="hidden" id="id" name="id" value="${company.id}">
		</div>
		<ul class="list-group">
		    <li class="list-group-item"><label class="control-label">Company name :</label> ${company.companyName}</li>
		    <li class="list-group-item"><label class="control-label">Company address :</label> ${company.address}</li>
		    <li class="list-group-item"><label class="control-label">Company logo :</label> <img  src="<c:url value="/resources/company/${company.id}.jpg" />" height="30px" width="100px" alt="..."></li>
		    <li class="list-group-item"><label class="control-label">Contact email address :</label> ${company.contactEmailAddress}</li>
		    <li class="list-group-item"><label class="control-label">Contact landline number :</label> ${company.contactLandlineNumber}</li>
		    <li class="list-group-item"><label class="control-label">Total employees :</label> ${company.employeeStrength}</li>
		</ul>
			<div class="form-group">
				<button type="button" id="btnEdit" class="btn btn-warning">Edit</button>
    			<button type="button" id="btnDelete" class="btn btn-danger">Delete</button>
    			<button type="button" id="btnAddNewProduct" class="btn btn-success">Add new product</button>
  			</div>
	</div>
	<div id="addProductDiv" style="display:none; width:90%; margin-left:5%;">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title"><h5>Add product for ${company.companyName}</h5></div>
			</div>
			
			<div class="panel-body">
				<c:choose>
					<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
						<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
					</c:when>
					<c:when test='${successMessage != null && !successMessage.equals("")}'>
						<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
					</c:when>
					<c:otherwise>
						<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
					</c:otherwise>
				</c:choose>
				<c:url value="/product/ajaxSave" var="addUrl"></c:url>
				<form:form action="${addUrl}" method="post" data-toggle="validator" role="form" commandName="product" modelAttribute="product">
					<div class="form-group">
						<form:input type="hidden" id="id" path="id" value="${product.id}" />
					</div>
					<div class="form-group">
						<label for="productName" class="control-label">Product name</label>
						<form:input type="text" maxlength="255" class="form-control" id="productName" value="${product.productName}" path="productName" placeholder="Product name" required="required" />
						<div class="help-block with-errors"></div>
						<form:errors path="productName" />
					</div>
					<div class="form-group">
						<label for="productCode"  class="control-label">Product code</label>
						<form:input type="text" maxlength="100" class="form-control" value="${product.productCode}" id="productCode" path="productCode" placeholder="Product code" required="required" />
						<div class="help-block with-errors"></div>
						<form:errors path="productCode" />
					</div>
					<div class="form-group">
						<label for="feature"  class="control-label">Product feature</label>
						<form:textarea rows="5" maxlength="1000" cols="50" class="form-control" id="feature" path="feature" placeholder="Product feature" required="required" ></form:textarea>
						<form:errors path="feature" />
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="image" class="control-label">Product image</label>
						<form:input type="text" maxlength="100" class="form-control" id="image" value="${product.image}" path="image" placeholder="image" required="required" />
						<div class="help-block with-errors"></div>
						<form:errors path="image" />
					</div>
					<div class="form-group">
						<input type="hidden" id="listCompanies" name="listCompanies" value="${company.id}" />
					</div>
					<div class="form-group">
						<form:button  type="submit" id="btnSave" class="btn btn-success">Save</form:button>
		    			<form:button type="button" id="btnCancelProduct" class="btn btn-danger">Cancel</form:button>
		  			</div>
				</form:form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
		
			$("#btnEdit").click(function(){
				document.location = '<c:url value="/company/edit/${company.id}" />';
		    });
			
			$("#btnDelete").click(function(){
				if(confirm("Are you sure to delete this company?")) {
					document.location = '<c:url value="/company/delete/${company.id}" />';
				}
		    });
			
			$("#btnCancel").click(function(){
		        document.location = '<c:url value="/products" />';
		    });
			
			$("#btnAddNewProduct").click(function(){
		        $("#addProductDiv").show();
		        $("#btnAddNewProduct").disable();
		    });
			
			$("#btnCancelProduct").click(function(){
		        $("#addProductDiv").hide();
		    });
			
		});
	</script>
</div>