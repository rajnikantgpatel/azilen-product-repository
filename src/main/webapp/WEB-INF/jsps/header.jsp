<%@page import="com.azilen.azilenProduct.model.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header class="navbar-inverse" role="banner">
	<div class="container">
		<nav role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="<c:url value="/" />">Azilen Product</a>
		    </div>
		
		    <!-- Collect the nav links, forms, and other content for toggling -->
			    	
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    <% User loggedInUser = (User) session.getAttribute("loggedInUser"); %>
				<c:choose>
					<c:when test='${loggedInUser != null}'>
						<ul class="nav navbar-nav">
							<li><a href="<c:url value="/company/companies" />">Companies</a></li>
						    <li><a href="<c:url value="/product/products" />">Products</a></li>
						    <li><a href="<c:url value="/user/users" />">Users</a></li>
						    <li><a href="<c:url value="/aboutUs" />">About us</a></li>
			      		</ul>
				      	<ul class="nav navbar-nav navbar-right">
							<li><div class="navbar-brand " style="color:#2B8C2F;" >Welcome ${loggedInUser.firstName} ${loggedInUser.lastName} ( ${loggedInUser.email} )</div><a class="navbar-brand nav-danger" href="<c:url value="/user/logout" />">logout</a></li>
						</ul>
					</c:when>
					<c:otherwise>
						<ul class="nav navbar-nav">
					    <li><a href="<c:url value="/aboutUs" />">About us</a></li>
					    </ul>
				      	<ul class="nav navbar-nav navbar-right">
							<li><a href="<c:url value="/login" />">Login</a></li>
						</ul>
					</c:otherwise>
				</c:choose>
			    
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	</div>
</header>