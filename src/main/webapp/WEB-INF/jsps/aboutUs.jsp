<div class="panel panel-primary">
	<div class="panel-heading">
		<div class="panel-title"><h1>About us!</h1></div>
	</div>
	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<div>
					<h2 style="color: #F38021;">2008</h2>
					<div>
						<p>Year of establishment</p>
					</div>
				</div>
			</li>
			<li class="list-group-item">

				<div>
					<h2 style="color: #F38021;">100+</h2>
					<div>
						<p>Experienced</p>
						<p>Professionals</p>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<div>
					<h2 style="color: #F38021;">100%</h2>
					<div>
						<p>Self-funded</p>
						<p>organization</p>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<div>
					<h2 style="color: #F38021;">Innovate</h2>
					<div>
						<p>We innovate to</p>
						<p>add value</p>
					</div>
				</div>
			</li>
			<li class="list-group-item">
				<div>
					<div>
						<div>
							<br>
							<p>Every business potential presents great challenges and the
								solution lies in the constant quest to innovate something
								better. As a high-end IT solutions &amp; services developer, we
								analyse and understand your business better. Because better
								innovation is not simply delivering just what your business
								needs so we believe in innovation that adds a value to the
								solution we deliver, we believe in innovation that benefits you
								better in long run. That's precisely what makes us different!</p>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
