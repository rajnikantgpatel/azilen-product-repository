<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="container">    
	<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-1 col-sm-8 col-sm-offset-2">                    
		<div class="panel panel-primary" >
			<div class="panel-heading">
				<div class="panel-title">Sign In to Azilen Product application</div>
			</div>
			<div style="padding-top:30px" class="panel-body" >
				<c:choose>
					<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
						<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
					</c:when>
					<c:when test='${successMessage != null && !successMessage.equals("")}'>
						<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
					</c:when>
					<c:otherwise>
						<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
					</c:otherwise>
				</c:choose>
				<c:url value="/user/login" var="loginUrl"/>
				<form:form id="loginform" action="${loginUrl}" method="post" class="form-horizontal" role="form" modelAttribute="userLogin" commandName="userLogin">
					<div style="margin-bottom: 25px" class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<form:input id="login-username" type="text" class="form-control" path="userNameOrEmail" placeholder="username or email" required="required" />                                        
					</div>
					<div style="margin-bottom: 25px" class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						<form:input id="login-password" type="password" class="form-control" path="password" placeholder="password" required="required" />
					</div>
					<div style="margin-top:10px" class="form-group">
						<!-- Button -->
						<div class="col-sm-12 controls">
							<button id="btn-login" type="submit" class="btn btn-success">Login  </button>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 control">
							<div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
								Don't have an account! 
								<a href="#" id="signuplink" >
									Sign Up Here
								</a>
							</div>
						</div>
					</div>    
				</form:form>     
			</div>                     
		</div>  
	</div>
	<div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-1 col-sm-8 col-sm-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">Sign Up to Azilen Product application</div>
				<div style="float:right; font-size: 85%; position: relative; top:-10px"><a style="color:#FFFFFF;" id="signinlink" href="#" >Sign In</a></div>
			</div>  
			<div class="panel-body" >
				<c:choose>
					<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
						<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
					</c:when>
					<c:when test='${successMessage != null && !successMessage.equals("")}'>
						<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
					</c:when>
					<c:otherwise>
						<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
					</c:otherwise>
				</c:choose>
				<c:url value="/user/signup" var="signUpUrl"/>
				<form:form id="signupform" action="${signUpUrl}" method="post" data-toggle="validator" class="form-horizontal" role="form" modelAttribute="user" commandName="user">
					<div id="signupalert" style="display:none" class="alert alert-danger">
						<p>Error:</p>
						<span></span>
					</div>
					<div class="form-group">
						<label for="email" class="col-md-3 control-label">Email *</label>
						<div class="col-md-9">
							<form:input type="email" maxlength="100" class="form-control" id="email" path="email" data-error="Hey, please enter valid email address for contact email address" placeholder="Email address" required="required" />
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="userName" class="col-md-3 control-label">User name *</label>
                          	<div class="col-md-9">
							<form:input type="text" maxlength="100" class="form-control" id="userName" path="userName" placeholder="user Name" required="required" />
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="firstName"  class="col-md-3 control-label">First Name *</label>
						<div class="col-md-9">
							<form:input type="text" maxlength="100" class="form-control" id="firstName" path="firstName" placeholder="First Name" required="required" />
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="lastName"  class="col-md-3 control-label">Last Name *</label>
						<div class="col-md-9">
							<form:input type="text" maxlength="100" class="form-control" id="lastName" path="lastName" placeholder="Last Name" required="required" />
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-3 control-label">Password *</label>
						<div class="col-md-9">
							<form:input type="password" data-minlength="6" class="form-control" maxlength="100" id="password" path="password" placeholder="password" required="required"/>
							<div class="help-block with-errors"></div>
							<input type="password" class="form-control" id="inputPasswordConfirm" data-match="#password" data-match-error="Whoops, these don't match" placeholder="Confirm password" required="required" />
							<div class="help-block with-errors"></div>
					    </div>
					</div>
					<div class="form-group">
						<!-- Button -->                                        
						<div class="col-md-offset-3 col-md-9">
							<form:button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp; Sign Up</form:button>
						</div>
					</div>
				</form:form>
			</div> 
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			<c:if test="${signUpError}">hideLoginBox();</c:if>

			function hideLoginBox() {
				$('#signupbox').hide();
				$('#loginbox').show();
			}
			
			$('#signinlink').click(function(){
				hideLoginBox();
			});
			
			$('#signuplink').click(function(){
				$('#loginbox').hide();
				$('#signupbox').show();
			});
		});
	</script>
</div>


