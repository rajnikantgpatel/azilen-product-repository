<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="panel panel-primary">
	<div class="panel-heading">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a style="color:#ffffff;" href="<c:url value="/product/products" />">Products</a>
			</div>
		</div>
		<div class="panel-title"><h1>Details of ${product.productName}</h1></div>
	</div>
	<div class="panel-body">
		<c:choose>
			<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
			</c:when>
			<c:when test='${successMessage != null && !successMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
			</c:when>
			<c:otherwise>
				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
			</c:otherwise>
		</c:choose>
		<div class="form-group">
			<input type="hidden" id="id" name="id" value="${product.id}">
		</div>
		<ul class="list-group">
		    <li class="list-group-item"><label class="control-label">Product name :</label> ${product.productName}</li>
		    <li class="list-group-item"><label class="control-label">Product code :</label> ${product.productCode}</li>
		    <li class="list-group-item"><label class="control-label">Product features :</label> ${product.feature}</li>
		    <li class="list-group-item"><label class="control-label">Image : </label> <img  src="<c:url value="/resources/product/${product.id}.jpg" />" height="30px" width="100px" alt="..."></li>
		    <li class="list-group-item"><label class="control-label">Company :</label> <a href="<c:url value="/company/show/${product.company.id}" />">${product.company.companyName}</a></li>
		</ul>
			<div class="form-group">
				<button type="button" id="btnEdit" class="btn btn-warning">Edit</button>
    			<button type="button" id="btnDelete" class="btn btn-danger">Delete</button>
  			</div>
	</div>
	<div class="panel-footer">
    				
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btnEdit").click(function(){
				document.location = '<c:url value="/product/edit/${product.id}" />';
		    });
			
			$("#btnDelete").click(function(){
				if(confirm("Are you sure to delete this product?")) {
					document.location = '<c:url value="/product/delete/${product.id}" />';
				}
		    });

		});
	</script>
</div>