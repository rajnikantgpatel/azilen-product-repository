<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-primary">
	<div class="panel-heading">Products</div>
	<div class="panel-body">
		<h2>List of products</h2>
		
		<div class="btn-group pull-left">
 			<c:url var="addUrl" value="/product/products"/>
				<form action="${addUrl}" method="get" id="productListForm" data-toggle="validator" role="form">
					<div class="form-group">
						<label for="company" class="col-md-3 control-label">Company</label>
						<div class="col-md-9 ">
							<select name="company" id="company" class="form-control" >
								<option value="0">Select company</option>
								<c:forEach items="${listCompanies}" var="companyObject">
									<c:choose>
									<c:when test="${companyObject.getId() == companyId}">
										<option selected="selected" value="${companyObject.getId()}" >${companyObject.getCompanyName()}</option>
									</c:when>
									<c:otherwise>
										<option value="${companyObject.getId()}" >${companyObject.getCompanyName()}</option>
									</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</form>
			</div>


			<div class="btn-group pull-right">
				<a href="<c:url value="/product/add" />">Add new product</a>
			</div>
			</div>
	<c:choose>
		<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
			<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
		</c:when>
		<c:when test='${successMessage != null && !successMessage.equals("")}'>
			<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
		</c:when>
		<c:otherwise>
			<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
		</c:otherwise>
	</c:choose>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Product Name</th>
					<th>Product code</th>
					<th>More details</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listProducts}" var="product">
					<tr>
						<td>${product.id}</td>
						<td>${product.productName}</td>
						<td>${product.productCode}</td>
						<td><a href="<c:url value="/product/show/${product.id}" />">Show details</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#company").change(function(){
		$('#productListForm').submit();
    });
});
</script>
