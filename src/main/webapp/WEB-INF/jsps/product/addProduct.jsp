<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="panel panel-primary">
	<div class="panel-heading">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a style="color:#ffffff;" href="<c:url value="/product/products" />">Products</a>
			</div>
		</div>
		<div class="panel-title"><h1>${action} product</h1></div>
	</div>
	<div class="panel-body">
		<c:choose>
			<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
			</c:when>
			<c:when test='${successMessage != null && !successMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
			</c:when>
			<c:otherwise>
				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
			</c:otherwise>
		</c:choose>
		<c:url var="addUrl" value="/product/save"/>
		<form:form action="${addUrl}" method="post" data-toggle="validator" role="form" commandName="product" modelAttribute="product" enctype="multipart/form-data">
			<div class="form-group">
				<form:input type="hidden" id="id" path="id" />
			</div>
			<div class="form-group">
				<label for="productName" class="col-md-3 control-label">Product name *</label>
				<div class="col-md-9">
					<form:input type="text" maxlength="255" class="form-control" id="productName" path="productName" placeholder="Product name" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="productCode"  class="col-md-3 control-label">Product code *</label>
				<div class="col-md-9">
					<form:input type="text" maxlength="100" class="form-control" id="productCode" path="productCode" placeholder="Product code" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="feature"  class="col-md-3 control-label">Product feature *</label>
				<div class="col-md-9">
					<form:textarea rows="5" maxlength="1000" cols="50" class="form-control" id="feature" path="feature" placeholder="Product feature" required="required" ></form:textarea>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="image" class="col-md-3 control-label">Product image *</label>
				<div class="col-md-9">
					<form:input type="file" id="file" class="form-control" placeholder="Product image" required="required" maxlength="100" path="file" />
					<%-- <form:input type="text" maxlength="100" class="form-control" id="image" path="image" placeholder="image" required="required" /> --%>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="company" class="col-md-3 control-label">Company *</label>
				<div class="col-md-9">
					<select name="companyId" class="form-control" >
						<option value="0">Select company</option>
						<c:forEach items="${listCompanies}" var="company">
							<c:choose>
								<c:when test="${product.company.id ==  company.id }">
									<option label="${company.companyName}" value="${company.id}" selected="selected"/>
								</c:when>
								<c:otherwise>
									<option label="${company.companyName}" value="${company.id}"/>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<form:button type="submit" id="btnSave" class="btn btn-success">Save</form:button>
    			<form:button type="button" id="btnCancel" class="btn btn-danger">Cancel</form:button>
  			</div>
		</form:form>
	</div>
	<div class="panel-footer">
    				
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btnCancel").click(function(){
		        document.location = '<c:url value="/product/products" />';
		    });
		});
	</script>
</div>