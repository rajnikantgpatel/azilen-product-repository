<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-primary">
	<div class="panel-heading">Users</div>
	<div class="panel-body">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a href="<c:url value="/user/add" />">Add new user</a>
			</div>
		</div>
		<h2>List of users</h2>
	</div>
	<c:choose>
		<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
			<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
		</c:when>
		<c:when test='${successMessage != null && !successMessage.equals("")}'>
			<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
		</c:when>
		<c:otherwise>
			<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
		</c:otherwise>
	</c:choose>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Email</th>
					<th>More details</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listUsers}" var="user">
					<tr>
						<td>${user.id}</td>
						<td>${user.email}</td>
						<td><a href="<c:url value="/user/show/${user.id}" />">Show details</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

