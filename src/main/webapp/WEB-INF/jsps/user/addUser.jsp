<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="panel panel-primary">
	<div class="panel-heading">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a style="color:#ffffff;" href="<c:url value="/user/users" />">Users</a>
			</div>
		</div>
		<div class="panel-title"><h1>${action} user</h1></div>
	</div>
	<div class="panel-body">
		<c:choose>
			<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
			</c:when>
			<c:when test='${successMessage != null && !successMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
			</c:when>
			<c:otherwise>
				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
			</c:otherwise>
		</c:choose>
		<c:url var="addUrl" value="/user/save"/>
		<form:form action="${addUrl}" method="post" data-toggle="validator" role="form" commandName="user" modelAttribute="user">
			<div class="form-group">
				<form:input type="hidden" id="id" path="id" />
			</div>
			<div class="form-group">
				<label for="email" class="col-md-3 control-label">Email *</label>
				<div class="col-md-9">
					<form:input type="email" maxlength="100" class="form-control" id="email" path="email" data-error="Hey, please enter valid email address for contact email address" placeholder="Email address" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="userName" class="col-md-3 control-label">User name *</label>
				<div class="col-md-9">
					<form:input type="text" maxlength="100" class="form-control" id="userName" path="userName" placeholder="User Name" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="firstName"  class="col-md-3 control-label">First Name *</label>
				<div class="col-md-9">
					<form:input type="text" maxlength="100" class="form-control" id="firstName" path="firstName" placeholder="First Name" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="lastName"  class="col-md-3 control-label">Last Name *</label>
				<div class="col-md-9">
					<form:input type="text" maxlength="100" class="form-control" id="lastName" path="lastName" placeholder="Last Name" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-md-3 control-label">Password *</label>
				<div class="col-md-9">
					<form:input type="password" class="form-control" maxlength="100" id="password" path="password" placeholder="password" required="required" />
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-group">
				<form:button type="submit" id="btnSave" class="btn btn-success">Save</form:button>
    			<form:button type="button" id="btnCancel" class="btn btn-danger">Cancel</form:button>
  			</div>
		</form:form>
	</div>
	<div class="panel-footer">
    				
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btnCancel").click(function(){
		        document.location = '<c:url value="/user/users" />';
		    });
		});
	</script>
</div>