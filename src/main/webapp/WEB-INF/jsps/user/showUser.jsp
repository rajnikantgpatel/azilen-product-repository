<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="panel panel-primary">
	<div class="panel-heading">
		<div class='btn-toolbar pull-right'>
			<div class='btn-group'>
				<a style="color:#ffffff;" href="<c:url value="/user/users" />">Users</a>
			</div>
		</div>
		<div class="panel-title"><h1>Details of ${user.firstName} ${user.lastName}</h1></div>
	</div>
	<div class="panel-body">
		<c:choose>
			<c:when test='${errorMessage != null && !errorMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-danger col-sm-12">${errorMessage}</div>
			</c:when>
			<c:when test='${successMessage != null && !successMessage.equals("")}'>
				<div style="display:block" id="login-alert" class="alert alert-success col-sm-12">${successMessage}</div>
			</c:when>
			<c:otherwise>
				<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
			</c:otherwise>
		</c:choose>
		<div class="form-group">
			<input type="hidden" id="id" name="id" value="${user.id}">
		</div>
		<ul class="list-group">
		    <li class="list-group-item"><label class="control-label">Email address :</label> ${user.email}</li>
		    <li class="list-group-item"><label class="control-label">User name :</label> ${user.userName}</li>
		    <li class="list-group-item"><label class="control-label">First name :</label> ${user.firstName}</li>
		    <li class="list-group-item"><label class="control-label">Last name :</label> ${user.lastName}</li>
		</ul>
		<div class="form-group">
			<button type="button" id="btnEdit" class="btn btn-warning">Edit</button>
   			<c:if test='${loggedInUser != null && loggedInUser.id != user.id}'>
   				<button type="button" id="btnDelete" class="btn btn-danger">Delete</button>
   			</c:if>
 		</div>
	</div>
	<div class="panel-footer">
    				
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btnEdit").click(function(){
				document.location = '<c:url value="/user/edit/${user.id}" />';
		    });
			
			$("#btnDelete").click(function(){
				if(confirm("Are you su`re to delete this user?")) {
					document.location = '<c:url value="/user/delete/${user.id}" />';
				}
		    });

		});
	</script>
</div>