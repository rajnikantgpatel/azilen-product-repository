<%@ page isErrorPage="true" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="panel panel-primary">
	<div class="panel-heading">
		<div class="panel-title"><h1>About us!</h1></div>
	</div>
	<div class="panel-body">
		<ul class="list-group">
			<li class="list-group-item">
				<div>
					<h3>Sorry an exception occured!</h3>  
  
					Exception is: <%= exception %>

					Sorry for incconvenience. Please <a href='<c:url value="/" />'>click here</a> to goto home.
				</div>
			</li>
		</ul>
	</div>
</div>