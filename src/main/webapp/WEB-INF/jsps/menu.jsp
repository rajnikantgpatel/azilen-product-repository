<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<ul style="list-style:none;line-height:28px;">
    <li>
    <spring:url value="/home" var="homeUrl" htmlEscape="true" />
        <a href="${homeUrl}">Home</a>
    </li>
    <li>
    <spring:url value="/companies" var="companyListUrl" htmlEscape="true" />
        <a href="${companyListUrl}">Company List</a>
    </li>
    
    <li>
    <spring:url value="/aboutUs" var="aboutUsUrl" htmlEscape="true" />
        <a href="${aboutUsUrl}">About us</a>
    </li>
</ul>