package com.azilen.azilenProduct.utils;

// TODO: Auto-generated Javadoc
/**
 * The Enum CompanyUniqueFieldEnum.
 *
 * @author Rajnikant Patel.
 */
public enum CompanyUniqueFieldEnum {
	
	/** The company name conflicting. */
	COMPANY_NAME_CONFLICTING(1),
	
	/** The contact email address conflicting. */
	CONTACT_EMAIL_ADDRESS_CONFLICTING(2),
	
	/** The contact landline number conflicting. */
	CONTACT_LANDLINE_NUMBER_CONFLICTING(3),
	
	/** The non conflicting. */
	NON_CONFLICTING(4);
	
	/** The value. */
	@SuppressWarnings("unused")
	private int value;

	/**
	 * Instantiates a new company unique field enum.
	 *
	 * @param value the value
	 */
	private CompanyUniqueFieldEnum(int value) {
		this.value = value;
	}
}
