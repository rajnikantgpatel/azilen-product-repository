package com.azilen.azilenProduct.utils;

import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

/**
 * The Class ExtendedMySQLDialect to add new function as Hibernate doesn't allow REGEXP function.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 22, 2015 3:39:44 PM
 */
public class ExtendedMySQLDialect extends MySQLDialect {

	/**
	 * Instantiates a new extended my sql dialect.
	 */
	public ExtendedMySQLDialect() {
		super();
		registerFunction( "regexp", new SQLFunctionTemplate(StandardBasicTypes.BOOLEAN, "?1 REGEXP ?2") );
	}
}
