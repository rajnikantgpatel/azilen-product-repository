package com.azilen.azilenProduct.utils;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceNotFoundException.
 *
 * @author Rajnikant Patel.
 * 
 */
public class ResourceNotFoundException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

}
