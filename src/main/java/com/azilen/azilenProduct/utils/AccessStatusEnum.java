package com.azilen.azilenProduct.utils;

// TODO: Auto-generated Javadoc
/**
 * The Enum AccessStatus to check the deleted or saved or updated or get status.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:53:29 AM
 */
public enum AccessStatusEnum {

	/** The deleted. */
	DELETED(1),

	/** The not accessible. */
	NOT_ACCESSIBLE(2),

	/** The reference exist. */
	REFERENCE_EXIST(3),

	/** The updated. */
	UPDATED(4),

	/** The saved. */
	SAVED(5),
	
	/** The company name conflicting. */
	COMPANY_COMPANY_NAME_CONFLICTING(6),
	
	/** The contact email address conflicting. */
	COMPANY_CONTACT_EMAIL_ADDRESS_CONFLICTING(7),
	
	/** The contact landline number conflicting. */
	COMPANY_CONTACT_LANDLINE_NUMBER_CONFLICTING(8),
	
	/** The product code conflicting. */
	PRODUCT_PRODUCT_CODE_CONFLICTING(9),
	
	/** The user email conflicting. */
	USER_EMAIL_CONFLICTING(10),
	
	/** The user user name conflicting. */
	USER_USER_NAME_CONFLICTING(11);
	
	/** The value. */
	@SuppressWarnings("unused")
	private int value;

	/**
	 * Instantiates a new access status.
	 *
	 * @param value
	 *            the value
	 */
	private AccessStatusEnum(int value) {
		this.value = value;
	}

}
