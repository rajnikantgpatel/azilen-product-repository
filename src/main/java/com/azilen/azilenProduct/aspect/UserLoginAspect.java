package com.azilen.azilenProduct.aspect;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.model.UserLogin;
import com.azilen.azilenProduct.service.UserService;

// TODO: Auto-generated Javadoc
/**
 * The Class UserLoginAspect to implement login process using AOP module.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:45:45 AM
 *
 */
@Aspect
public class UserLoginAspect {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(UserLoginAspect.class);

	/** The user service. */
	private UserService userService;

	/**
	 * Sets the user service.
	 *
	 * @param userService
	 *            the new user service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * This method is used to create the session with the credentials provided by user from login page.
	 *
	 * @param joinPoint
	 *            the join point
	 * @param request
	 *            the request
	 * @param userLogin
	 *            the user login
	 * @param model
	 *            the model
	 */
	@Before("execution(* com.azilen.azilenProduct.controller.UserController.userLogin(..)) && args(request, userLogin, model))")
	// HttpServletRequest request, @ModelAttribute("userLogin") UserLogin userLogin, Model model
	public void loginBefore(JoinPoint joinPoint, HttpServletRequest request, @ModelAttribute("userLogin") UserLogin userLogin, Model model) {

		logger.info("userNameOrEmail : " + userLogin.getUserNameOrEmail());
		User userTryingToLogin = this.userService.getUserByUserNameOrEmailAndPassword(userLogin);

		if (userTryingToLogin != null) {
			HttpSession session = request.getSession();
			session.setAttribute("loggedInUser", userTryingToLogin);
		}
	}
}
