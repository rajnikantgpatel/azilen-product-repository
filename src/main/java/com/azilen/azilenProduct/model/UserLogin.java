package com.azilen.azilenProduct.model;

// TODO: Auto-generated Javadoc
/**
 * The Class UserLogin for login page login process.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 22, 2015 10:57:00 AM
 *
 */
public class UserLogin {

	/** The user name or email. */
	private String userNameOrEmail;

	/** The password. */
	private String password;

	/**
	 * Instantiates a new user login.
	 */
	public UserLogin() {
		userNameOrEmail = "";
		password = "";
	}

	/**
	 * Gets the user name or email.
	 *
	 * @return the user name or email
	 */
	public String getUserNameOrEmail() {
		return userNameOrEmail;
	}

	/**
	 * Sets the user name or email.
	 *
	 * @param userNameOrEmail
	 *            the new user name or email
	 */
	public void setUserNameOrEmail(String userNameOrEmail) {
		this.userNameOrEmail = userNameOrEmail;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
