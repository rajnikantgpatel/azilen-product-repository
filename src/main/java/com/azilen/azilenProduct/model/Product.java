package com.azilen.azilenProduct.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

// TODO: Auto-generated Javadoc
/**
 * This is a model class for product table of MySql database. This is used to get/set persistant record data of table product using hibernate session.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 12, 2015 6:37:01 PM
 *
 */
@Entity
@Table(name = "product")
public class Product {

	/** The id. */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	/** The product name. */
	@Column(name = "product_name", length = 255, nullable = false, updatable = true)
	private String productName;

	/** The product code. */
	@Column(name = "product_code", unique = true, length = 100, nullable = false, updatable = true)
	private String productCode;

	/** The feature. */
	@Column(name = "feature", length = 1000, nullable = false, updatable = true)
	private String feature;

	/** The image. */
	@Column(name = "image", length = 100, nullable = false, updatable = true)
	private String image;

	/** The company. */
	@ManyToOne
	@JoinColumn(name = "company_id", nullable = false)
	private Company company;

	/** The file. */
	@Transient
	private MultipartFile file;

	/**
	 * Instantiates a new product.
	 */
	public Product() {
		id = 0;
		productName = "";
		productCode = "";
		feature = "";
		image = "";
		company = null;
		file = null;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName
	 *            the new product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * Gets the product code.
	 *
	 * @return the product code
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * Sets the product code.
	 *
	 * @param productCode
	 *            the new product code
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * Gets the feature.
	 *
	 * @return the feature
	 */
	public String getFeature() {
		return feature;
	}

	/**
	 * Sets the feature.
	 *
	 * @param feature
	 *            the new feature
	 */
	public void setFeature(String feature) {
		this.feature = feature;
	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Sets the image.
	 *
	 * @param image
	 *            the new image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	public Company getCompany() {
		return company;
	}

	/**
	 * Sets the company.
	 *
	 * @param company
	 *            the new company
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 *
	 * @param file
	 *            the new file
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}
}
