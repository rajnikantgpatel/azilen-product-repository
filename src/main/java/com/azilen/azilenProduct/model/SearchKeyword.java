package com.azilen.azilenProduct.model;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchKeyword.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 22, 2015 12:22:39 PM
 */
public class SearchKeyword {

	/** The search company or product keywords. */
	private String searchCompanyOrProductKeywords;

	/**
	 * Instantiates a new search keyword.
	 */
	public SearchKeyword() {
		searchCompanyOrProductKeywords = "";
	}

	/**
	 * Gets the search company or product keywords.
	 *
	 * @return the search company or product keywords
	 */
	public String getSearchCompanyOrProductKeywords() {
		return searchCompanyOrProductKeywords;
	}

	/**
	 * Sets the search company or product keywords.
	 *
	 * @param searchCompanyOrProductKeywords
	 *            the new search company or product keywords
	 */
	public void setSearchCompanyOrProductKeywords(String searchCompanyOrProductKeywords) {
		this.searchCompanyOrProductKeywords = searchCompanyOrProductKeywords;
	}

	/**
	 * Gets the seperated keyword string.
	 *
	 * @return the seperated keyword string
	 */
	public String getSeperatedKeywordString() {

		String[] searchCompanyOrProductKeywordsArray = searchCompanyOrProductKeywords.split(",");
		String searchCompanyOrProductKeywordsList = "";
		int keywordCounter = 0;
		for (String keyword : searchCompanyOrProductKeywordsArray) {
			String[] keywordArray = keyword.split(" ");
			if (keywordArray != null && !keywordArray.equals(""))
				for (String internalkeyword : keywordArray) {
					if (internalkeyword != null && !internalkeyword.equals("")) {
						searchCompanyOrProductKeywordsList += ((keywordCounter > 0) ? "|" : "") + internalkeyword;
						keywordCounter++;
					}
				}
		}

		return searchCompanyOrProductKeywordsList;
	}
}
