package com.azilen.azilenProduct.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

// TODO: Auto-generated Javadoc
/**
 * This is a model class for company table of MySql database. This is used to get persistant record data of table company using hibernate session.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 12, 2015 4:04:36 PM
 *
 */

@Entity
@Table(name = "company")
public class Company {

	/** The id. */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	/** The company name. */
	@Column(name = "company_name", unique = true, length = 255, nullable = false, updatable = true)
	private String companyName;

	/** The address. */
	@Column(name = "address", length = 1000, nullable = false, updatable = true)
	private String address;

	/** The logo. */
	@Column(name = "logo", length = 100, nullable = false, updatable = true)
	private String logo;

	/** The contact email address. */
	@Column(name = "contact_email_address", unique = true, length = 100, nullable = false, updatable = true)
	private String contactEmailAddress;

	/** The contact landline number. */
	@Column(name = "contact_landline_number", unique = true, length = 15, nullable = false, updatable = true)
	private String contactLandlineNumber;

	/** The employee strength. */
	@Column(name = "employee_strength", length = 5, nullable = false, updatable = true)
	private int employeeStrength;

	/** The products. */
	@OneToMany(mappedBy = "company")
	private Set<Product> products;

	/** The file. */
	@Transient
	private MultipartFile file;

	/**
	 * Instantiates a new company.
	 */
	public Company() {
		id = 0;
		companyName = "";
		address = "";
		logo = "";
		contactEmailAddress = "";
		contactLandlineNumber = "";
		employeeStrength = 0;
		file = null;
	}

	/** The user. */
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the company name.
	 *
	 * @param companyName
	 *            the new company name
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address
	 *            the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the logo.
	 *
	 * @return the logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * Sets the logo.
	 *
	 * @param logo
	 *            the new logo
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * Gets the contact email address.
	 *
	 * @return the contact email address
	 */
	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	/**
	 * Sets the contact email address.
	 *
	 * @param contactEmailAddress
	 *            the new contact email address
	 */
	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	/**
	 * Gets the contact landline number.
	 *
	 * @return the contact landline number
	 */
	public String getContactLandlineNumber() {
		return contactLandlineNumber;
	}

	/**
	 * Sets the contact landline number.
	 *
	 * @param contactLandlineNumber
	 *            the new contact landline number
	 */
	public void setContactLandlineNumber(String contactLandlineNumber) {
		this.contactLandlineNumber = contactLandlineNumber;
	}

	/**
	 * Gets the employee strength.
	 *
	 * @return the employee strength
	 */
	public int getEmployeeStrength() {
		return employeeStrength;
	}

	/**
	 * Sets the employee strength.
	 *
	 * @param employeeStrength
	 *            the new employee strength
	 */
	public void setEmployeeStrength(int employeeStrength) {
		this.employeeStrength = employeeStrength;
	}

	/**
	 * Gets the products.
	 *
	 * @return the products
	 */
	public Set<Product> getProducts() {
		return products;
	}

	/**
	 * Sets the products.
	 *
	 * @param products
	 *            the new products
	 */
	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user
	 *            the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 *
	 * @param file
	 *            the new file
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

}
