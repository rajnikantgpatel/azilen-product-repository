package com.azilen.azilenProduct.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * This is a model class for user table of MySql database. This is used to get/set persistant record data of table user using hibernate session.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 12, 2015 6:37:01 PM
 *
 */
@Entity
@Table(name = "user")
public class User {

	/** The id. */
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	/** The email. */
	@Column(name = "email", unique= true, length = 100, nullable = false, updatable = true)
	private String email;

	/** The user name. */
	@Column(name = "user_name", unique= true, length = 100, nullable = false, updatable = true)
	private String userName;

	/** The first name. */
	@Column(name = "first_name", length = 100, nullable = false, updatable = true)
	private String firstName;

	/** The last name. */
	@Column(name = "last_name", length = 1000, nullable = false, updatable = true)
	private String lastName;

	/** The password. */
	@Column(name = "password", length = 100, nullable = false, updatable = true)
	private String password;

	/** The companies. */
	@OneToMany(mappedBy = "user")
	private Set<Company> companies;

	/**
	 * Instantiates a new user.
	 */
	public User() {
		id = 0;
		email = "";
		userName = "";
		firstName = "";
		lastName = "";
		password = "";
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName
	 *            the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName
	 *            the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the companies.
	 *
	 * @return the companies
	 */
	public Set<Company> getCompanies() {
		return companies;
	}

	/**
	 * Sets the companies.
	 *
	 * @param companies
	 *            the new companies
	 */
	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}
}
