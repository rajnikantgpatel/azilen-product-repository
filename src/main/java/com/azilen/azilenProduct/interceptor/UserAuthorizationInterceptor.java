package com.azilen.azilenProduct.interceptor;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.azilen.azilenProduct.dao.CompanyDAOImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class UserAuthorizationInterceptor to restrict to see unauthorized pages if user is not logged in.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:37:09 AM
 */
public class UserAuthorizationInterceptor extends HandlerInterceptorAdapter {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(CompanyDAOImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String requestURL = request.getRequestURL().toString();
		logger.info("Request URL::" + requestURL + ":: Start Time=" + System.currentTimeMillis());
		HttpSession session = request.getSession();
		if (!(requestURL.endsWith("AzilenProduct/"))) {
			if (session.getAttribute("loggedInUser") == null) {
				request.setAttribute("notLoggedIn", "You are not authorized to view the requested page. Please do login.");
				RequestDispatcher objRequestDispatcher = request.getRequestDispatcher("/");
				objRequestDispatcher.forward(request, response);
				return false;
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		logger.info("Request URL::" + request.getRequestURL().toString() + " Sent to Handler :: Current Time=" + System.currentTimeMillis());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

	}
}
