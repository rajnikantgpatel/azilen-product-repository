package com.azilen.azilenProduct.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class CompanyDAOImpl is the implementation of CompanyDAO interface.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:32:09 AM
 *
 */
@Repository
public class CompanyDAOImpl implements CompanyDAO {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(CompanyDAOImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sf
	 *            the new session factory
	 */
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.CompanyDAO#saveCompany(com.azilen.azilenProduct.model.Company)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public AccessStatusEnum saveCompany(Company company) {
		Session session = null;
		AccessStatusEnum accessStatusEnum = AccessStatusEnum.SAVED;
		try {
			session = this.sessionFactory.openSession();
			List<Company> companiesFromDatabaseToCheckUnique = (List<Company>) session.createQuery(
					"FROM Company c WHERE c.companyName = '" + company.getCompanyName() + "' OR c.contactEmailAddress = '" + company.getContactEmailAddress() + "' OR c.contactLandlineNumber = '"
							+ company.getContactLandlineNumber() + "'").list();

			if (companiesFromDatabaseToCheckUnique != null && !companiesFromDatabaseToCheckUnique.isEmpty()) {
				Company companyFromDatabaseToCheckUnique = companiesFromDatabaseToCheckUnique.get(0);
				if (companyFromDatabaseToCheckUnique != null) {
					if (companyFromDatabaseToCheckUnique.getCompanyName().equals(company.getCompanyName())) {
						accessStatusEnum = AccessStatusEnum.COMPANY_COMPANY_NAME_CONFLICTING;
					} else if (companyFromDatabaseToCheckUnique.getContactEmailAddress().equals(company.getContactEmailAddress())) {
						accessStatusEnum = AccessStatusEnum.COMPANY_CONTACT_EMAIL_ADDRESS_CONFLICTING;
					} else if (companyFromDatabaseToCheckUnique.getContactLandlineNumber().equals(company.getContactLandlineNumber())) {
						accessStatusEnum = AccessStatusEnum.COMPANY_CONTACT_LANDLINE_NUMBER_CONFLICTING;
					}
				}
			}

			if (accessStatusEnum == AccessStatusEnum.SAVED) {
				session.beginTransaction();
				company.setLogo("0.jpg");
				session.save(company);
				company.setLogo(company.getId() + ".jpg");
				session.update(company);
				session.getTransaction().commit();
				logger.info("Company saved successfully, Company Details=" + company);
			}
		} catch (Exception e) {
			logger.error("Company save::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return accessStatusEnum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.CompanyDAO#updateCompany(com.azilen. azilenProduct.model.Company)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public AccessStatusEnum updateCompany(Company company) {
		Session session = null;
		AccessStatusEnum accessStatusEnum = AccessStatusEnum.UPDATED;
		try {
			session = this.sessionFactory.openSession();
			Company companyFromDatabase = (Company) session.get(Company.class, new Integer(company.getId()));
			if (companyFromDatabase.getUser().getId() != company.getUser().getId()) {
				accessStatusEnum = AccessStatusEnum.NOT_ACCESSIBLE;
				logger.info("Company was not updated successfully as it was not accessible to the user : " + company.getUser().getEmail() + ", Company Details=" + company);
			} else {

				List<Company> companiesFromDatabaseToCheckUnique = (List<Company>) session.createQuery(
						"FROM Company c WHERE (c.companyName = '" + company.getCompanyName() + "' OR c.contactEmailAddress = '" + company.getContactEmailAddress() + "' OR c.contactLandlineNumber = '"
								+ company.getContactLandlineNumber() + "') AND c.id != " + company.getId()).list();

				if (companiesFromDatabaseToCheckUnique != null && !companiesFromDatabaseToCheckUnique.isEmpty()) {
					Company companyFromDatabaseToCheckUnique = companiesFromDatabaseToCheckUnique.get(0);
					if (companyFromDatabaseToCheckUnique != null) {
						if (companyFromDatabaseToCheckUnique.getCompanyName().equals(company.getCompanyName())) {
							accessStatusEnum = AccessStatusEnum.COMPANY_COMPANY_NAME_CONFLICTING;
						} else if (companyFromDatabaseToCheckUnique.getContactEmailAddress().equals(company.getContactEmailAddress())) {
							accessStatusEnum = AccessStatusEnum.COMPANY_CONTACT_EMAIL_ADDRESS_CONFLICTING;
						} else if (companyFromDatabaseToCheckUnique.getContactLandlineNumber().equals(company.getContactLandlineNumber())) {
							accessStatusEnum = AccessStatusEnum.COMPANY_CONTACT_LANDLINE_NUMBER_CONFLICTING;
						}
					}
				}

				if (accessStatusEnum == AccessStatusEnum.UPDATED) {
					session.beginTransaction();
					session.update(company);
					session.getTransaction().commit();
					logger.info("Company updated successfully, Company Details=" + company);
				}
			}
		} catch (Exception e) {
			logger.error("Company Update::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return accessStatusEnum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.CompanyDAO#listCompanies(com.azilen. azilenProduct.model.User)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> listCompanies(User loggedInUser) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			List<Company> CompaniesList = session.createQuery("FROM Company c WHERE c.user.id = " + loggedInUser.getId()).list();
			for (Company company : CompaniesList) {
				logger.info("Company List::" + company);
			}
			return CompaniesList;
		} catch (Exception e) {
			logger.error("Company List::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.CompanyDAO#getCompanyById(int)
	 */
	@Override
	public Company getCompanyById(int id) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Company company = (Company) session.get(Company.class, id);
			logger.info("Company loaded successfully, Company details=" + company);
			return company;
		} catch (Exception e) {
			logger.error("Company get by id::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.CompanyDAO#deleteCompany(int, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public AccessStatusEnum deleteCompany(int id, User loggedInUser) {
		Session session = null;
		AccessStatusEnum accessStatus = AccessStatusEnum.DELETED;
		try {
			session = this.sessionFactory.openSession();
			Company company = (Company) session.get(Company.class, new Integer(id));

			if (company != null) {
				if (company.getUser().getId() != loggedInUser.getId()) {
					accessStatus = AccessStatusEnum.NOT_ACCESSIBLE;
					logger.info("Company wasn't deleted as it was not accessible for user : " + loggedInUser.getEmail() + " , Company details=" + company);
				} else if (company.getProducts() != null && !company.getProducts().isEmpty()) {
					logger.info("Company wasn't deleted as it has some products which are associated with it, Company details=" + company);
					accessStatus = AccessStatusEnum.REFERENCE_EXIST;
				} else {
					session.beginTransaction();
					session.delete(company);
					session.getTransaction().commit();
					logger.info("Company was deleted successfully, Company details=" + company);
				}
			}
		} catch (Exception e) {
			logger.error("Company delete ::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}

		return accessStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.CompanyDAO#searchCompaniesUsingKeywords( java.lang.String, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public List<Company> searchCompaniesUsingKeywords(String keywords, User loggedInUser) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			@SuppressWarnings("unchecked")
			List<Company> companies = (List<Company>) session.createQuery(
					"FROM Company as c where (regexp(c.companyName, '" + keywords + "') <> 0 OR regexp(c.address, '" + keywords + "') <> 0 OR regexp(c.contactEmailAddress, '" + keywords
							+ "') <> 0 OR regexp(c.contactLandlineNumber, '" + keywords + "') <> 0) AND c.user.id = " + loggedInUser.getId()).list();
			logger.info("Companies loaded successfully for the searched keywords, Total companies found :=" + ((companies != null && !companies.isEmpty()) ? companies.size() : 0));
			return companies;
		} catch (Exception e) {
			logger.error("search Companies Using Keywords" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.CompanyDAO#getCompanyByContactEmailAddress(java.lang.String)
	 */
	@Override
	public Company getCompanyByContactEmailAddress(String contactEmailAddress) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Company company = (Company) session.createQuery("FROM Company c WHERE c.contactEmailAddress = '" + contactEmailAddress + "'").uniqueResult();
			logger.info("Company loaded successfully using contact email address, Company details=" + company);
			return company;
		} catch (Exception e) {
			logger.error("Get Company By Contact Email Address :: " + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}
}
