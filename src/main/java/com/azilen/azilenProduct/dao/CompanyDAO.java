package com.azilen.azilenProduct.dao;

import java.util.List;

import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Interface CompanyDAO to serve company service class methods.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:29:47 AM
 *
 */
public interface CompanyDAO {

	/**
	 * Save company.
	 *
	 * @param company the company
	 * @return the company unique field enum
	 */
	public AccessStatusEnum saveCompany(Company company);

	/**
	 * Update company.
	 *
	 * @param company
	 *            the company
	 * @return the access status
	 */
	public AccessStatusEnum updateCompany(Company company);

	/**
	 * List companies.
	 *
	 * @param loggedInUser
	 *            the logged in user
	 * @return the list
	 */
	public List<Company> listCompanies(User loggedInUser);

	/**
	 * Gets the company by id.
	 *
	 * @param id
	 *            the id
	 * @return the company by id
	 */
	public Company getCompanyById(int id);

	/**
	 * Delete company.
	 *
	 * @param id
	 *            the id
	 * @param loggedInUser
	 *            the logged in user
	 * @return the access status
	 */
	public AccessStatusEnum deleteCompany(int id, User loggedInUser);

	/**
	 * Search companies using keywords.
	 *
	 * @param keywords
	 *            the keywords
	 * @param loggedInUser
	 *            the logged in user
	 * @return the list
	 */
	public List<Company> searchCompaniesUsingKeywords(String keywords, User loggedInUser);

	/**
	 * Gets the company by contact email address.
	 *
	 * @param contactEmailAddress the contact email address
	 * @return the company by contact email address
	 */
	public Company getCompanyByContactEmailAddress(String contactEmailAddress);
}
