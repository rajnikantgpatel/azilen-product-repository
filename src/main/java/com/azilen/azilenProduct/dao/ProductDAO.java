package com.azilen.azilenProduct.dao;

import java.util.List;

import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.Product;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Interface ProductDAO to serve the product service class's methods.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:33:02 AM
 *
 */
public interface ProductDAO {

	/**
	 * Save product.
	 *
	 * @param product
	 *            the product
	 * @return the access status enum
	 */
	public AccessStatusEnum saveProduct(Product product);

	/**
	 * Update product.
	 *
	 * @param product
	 *            the product
	 * @return the access status enum
	 */
	public AccessStatusEnum updateProduct(Product product);

	/**
	 * List products.
	 *
	 * @param loggedInUser
	 *            the logged in user
	 * @return the list
	 */
	public List<Product> listProducts(User loggedInUser);

	/**
	 * Gets the product by id.
	 *
	 * @param id
	 *            the id
	 * @return the product by id
	 */
	public Product getProductById(int id);

	/**
	 * Delete product.
	 *
	 * @param id
	 *            the id
	 * @param loggedInUser
	 *            the logged in user
	 * @return the access status
	 */
	public AccessStatusEnum deleteProduct(int id, User loggedInUser);

	/**
	 * Search products using keywords.
	 *
	 * @param keywords
	 *            the keywords
	 * @param loggedInUser
	 *            the logged in user
	 * @return the list
	 */
	public List<Product> searchProductsUsingKeywords(String keywords, User loggedInUser);

	/**
	 * List products for selected company.
	 *
	 * @param company
	 *            the company
	 * @param loggedInUser
	 *            the logged in user
	 * @return the list
	 */
	public List<Product> listProductsForSelectedCompany(Company company, User loggedInUser);

	/**
	 * Gets the product by product code.
	 *
	 * @param productCode
	 *            the product code
	 * @return the product by product code
	 */
	public Product getProductByProductCode(String productCode);
}
