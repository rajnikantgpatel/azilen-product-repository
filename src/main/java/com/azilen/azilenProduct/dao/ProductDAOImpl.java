package com.azilen.azilenProduct.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.Product;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class ProductDAOImpl to implement the ProductDAO interface.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:33:57 AM
 *
 */
@Repository
public class ProductDAOImpl implements ProductDAO {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(ProductDAOImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sf
	 *            the new session factory
	 */
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.ProductDAO#saveProduct(com.azilen.azilenProduct.model.Product)
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public AccessStatusEnum saveProduct(Product product) {
		Session session = null;
		AccessStatusEnum accessStatusEnum = AccessStatusEnum.SAVED;
		try {
			session = this.sessionFactory.openSession();
			List<Product> productsFromDatabaseToCheckUnique = (List<Product>) session.createQuery("FROM Product p WHERE p.productCode = '" + product.getProductCode() + "'").list();
			if (productsFromDatabaseToCheckUnique != null && !productsFromDatabaseToCheckUnique.isEmpty()) {
				Product productFromDatabaseToCheckUnique = productsFromDatabaseToCheckUnique.get(0);
				if (productFromDatabaseToCheckUnique != null) {
					if (productFromDatabaseToCheckUnique.getProductCode().equals(product.getProductCode())) {
						accessStatusEnum = AccessStatusEnum.PRODUCT_PRODUCT_CODE_CONFLICTING;
					}
				}
			}

			if (accessStatusEnum == AccessStatusEnum.SAVED) {
				session.beginTransaction();
				product.setImage("0.jpg");
				session.save(product);
				product.setImage(product.getId() + ".jpg");
				session.update(product);
				session.getTransaction().commit();
				logger.info("Product saved successfully, Product Details=" + product);
			}
		} catch (Exception e) {
			logger.error("Product save:: " + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return accessStatusEnum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.ProductDAO#updateProduct(com.azilen.azilenProduct.model.Product)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public AccessStatusEnum updateProduct(Product product) {
		Session session = null;
		AccessStatusEnum accessStatusEnum = AccessStatusEnum.UPDATED;
		try {
			session = this.sessionFactory.openSession();
			List<Product> productsFromDatabaseToCheckUnique = (List<Product>) session.createQuery(
					"FROM Product p WHERE p.productCode = '" + product.getProductCode() + "' AND p.id != " + product.getId()).list();
			if (productsFromDatabaseToCheckUnique != null && !productsFromDatabaseToCheckUnique.isEmpty()) {
				Product productFromDatabaseToCheckUnique = productsFromDatabaseToCheckUnique.get(0);
				if (productFromDatabaseToCheckUnique != null) {
					if (productFromDatabaseToCheckUnique.getProductCode().equals(product.getProductCode())) {
						accessStatusEnum = AccessStatusEnum.PRODUCT_PRODUCT_CODE_CONFLICTING;
					}
				}
			}

			if (accessStatusEnum == AccessStatusEnum.UPDATED) {
				session.beginTransaction();
				session.update(product);
				session.getTransaction().commit();
				logger.info("Product updated successfully, Product Details=" + product);
			}
		} catch (

		Exception e)

		{
			logger.error("Product Update:: " + e);
		} finally

		{
			if (session != null && session.isOpen())
				session.close();
		}
		return accessStatusEnum;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.ProductDAO#listProducts(com.azilen.azilenProduct.model.User)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> listProducts(User loggedInUser) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			List<Product> ProductsList = session.createQuery("FROM Product p WHERE p.company.user.id = " + loggedInUser.getId()).list();
			for (Product product : ProductsList) {
				logger.info("Product List::" + product);
			}
			return ProductsList;
		} catch (Exception e) {
			logger.error("Product List:: " + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.ProductDAO#getProductById(int)
	 */
	@Override
	public Product getProductById(int id) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Product product = (Product) session.get(Product.class, new Integer(id));
			logger.info("Product loaded successfully, Product details=" + product);
			return product;
		} catch (Exception e) {
			logger.error("Product get by id:: " + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.ProductDAO#deleteProduct(int, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public AccessStatusEnum deleteProduct(int id, User loggedInUser) {
		Session session = null;
		AccessStatusEnum accessStatus = AccessStatusEnum.DELETED;
		try {
			session = this.sessionFactory.openSession();
			Product product = (Product) session.get(Product.class, new Integer(id));

			if (product != null) {
				if (product.getCompany().getUser().getId() != loggedInUser.getId()) {
					accessStatus = AccessStatusEnum.NOT_ACCESSIBLE;
					logger.info("Product wasn't deleted as it was not accessible for user : " + loggedInUser.getEmail() + " , Product details=" + product);
				} else {
					session.beginTransaction();
					session.delete(product);
					session.getTransaction().commit();
					logger.info("Product was deleted successfully, Product details=" + product);
				}
			}
		} catch (Exception e) {
			logger.error("Product delete ::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}

		return accessStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.ProductDAO#searchProductsUsingKeywords(java.lang.String, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public List<Product> searchProductsUsingKeywords(String keywords, User loggedInUser) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			@SuppressWarnings("unchecked")
			List<Product> products = (List<Product>) session.createQuery(
					"FROM Product as p where (regexp(p.productName ,'" + keywords + "') <> 0 OR regexp(p.productCode, '" + keywords + "') <> 0 OR regexp(p.feature, '" + keywords
							+ "') <> 0) AND p.company.user.id = " + loggedInUser.getId()).list();
			logger.info("Products loaded successfully for the searched keywords, Total products found :=" + ((products != null && !products.isEmpty()) ? 0 : products.size()));
			return products;
		} catch (Exception e) {
			logger.error("search Products Using Keywords:: " + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.ProductDAO#listProductsForSelectedCompany(com.azilen.azilenProduct.model.Company, com.azilen.azilenProduct.model.User)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> listProductsForSelectedCompany(Company company, User loggedInUser) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			List<Product> ProductsList = session.createQuery("FROM Product p WHERE p.company.id = " + company.getId() + " AND p.company.user.id = " + loggedInUser.getId()).list();
			for (Product product : ProductsList) {
				logger.info("Product List for selected company::" + product);
			}
			return ProductsList;
		} catch (Exception e) {
			logger.error("Product List:: " + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.azilen.azilenProduct.dao.ProductDAO#getProductByProductCode(java.lang.String)
	 */
	@Override
	public Product getProductByProductCode(String productCode) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Product product = (Product) session.createQuery("FROM Product p WHERE p.productCode = '" + productCode + "'").uniqueResult();
			logger.info("Product loaded successfully using product code, Product details=" + product);
			return product;
		} catch (Exception e) {
			logger.error("Get Product By Product code :: " + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}
}
