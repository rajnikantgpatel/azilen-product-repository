package com.azilen.azilenProduct.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.model.UserLogin;
import com.azilen.azilenProduct.utils.AccessStatusEnum;
import com.azilen.azilenProduct.utils.EncryptionDecryptionUtility;

// TODO: Auto-generated Javadoc
/**
 * The Class UserDAOImpl to implement UserDao interface.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:36:03 AM
 *
 */
@Repository
public class UserDAOImpl implements UserDAO {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Sets the session factory.
	 *
	 * @param sf
	 *            the new session factory
	 */
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	/** The encryption decryption utility. */
	private EncryptionDecryptionUtility encryptionDecryptionUtility;

	/**
	 * Sets the encryption decryption utility.
	 *
	 * @param encryptionDecryptionUtility
	 *            the new encryption decryption utility
	 */
	public void setEncryptionDecryptionUtility(EncryptionDecryptionUtility encryptionDecryptionUtility) {
		this.encryptionDecryptionUtility = encryptionDecryptionUtility;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.UserDAO#saveUser(com.azilen.azilenProduct.model.User)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public AccessStatusEnum saveUser(User user) {
		Session session = null;
		AccessStatusEnum accessStatusEnum = AccessStatusEnum.SAVED;
		try {
			session = this.sessionFactory.openSession();
			List<User> usersFromDatabaseToCheckUnique = (List<User>) session.createQuery("FROM User u WHERE u.email = '" + user.getEmail() + "' OR u.userName = '" + user.getUserName() + "'").list();

			if (usersFromDatabaseToCheckUnique != null && !usersFromDatabaseToCheckUnique.isEmpty()) {
				User userFromDatabaseToCheckUnique = usersFromDatabaseToCheckUnique.get(0);
				if (userFromDatabaseToCheckUnique != null) {
					if (userFromDatabaseToCheckUnique.getEmail().equals(user.getEmail())) {
						accessStatusEnum = AccessStatusEnum.USER_EMAIL_CONFLICTING;
					} else if (userFromDatabaseToCheckUnique.getUserName().equals(user.getUserName())) {
						accessStatusEnum = AccessStatusEnum.USER_USER_NAME_CONFLICTING;
					}
				}
			}

			if (accessStatusEnum == AccessStatusEnum.SAVED) {
				session.beginTransaction();
				user.setPassword(encryptionDecryptionUtility.encrypt(user.getPassword()));
				session.save(user);
				session.getTransaction().commit();
				logger.info("User saved successfully, User Details=" + user);
			}
		} catch (Exception e) {
			logger.error("User save::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return accessStatusEnum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.UserDAO#updateUser(com.azilen.azilenProduct.model.User)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public AccessStatusEnum updateUser(User user) {
		AccessStatusEnum accessStatusEnum = AccessStatusEnum.UPDATED;
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			List<User> usersFromDatabaseToCheckUnique = (List<User>) session.createQuery("FROM User u WHERE (u.email = '" + user.getEmail() + "' OR u.userName = '" + user.getUserName() + "') AND u.id != " + user.getId()).list();

			if (usersFromDatabaseToCheckUnique != null && !usersFromDatabaseToCheckUnique.isEmpty()) {
				User userFromDatabaseToCheckUnique = usersFromDatabaseToCheckUnique.get(0);
				if (userFromDatabaseToCheckUnique != null) {
					if (userFromDatabaseToCheckUnique.getEmail().equals(user.getEmail())) {
						accessStatusEnum = AccessStatusEnum.USER_EMAIL_CONFLICTING;
					} else if (userFromDatabaseToCheckUnique.getUserName().equals(user.getUserName())) {
						accessStatusEnum = AccessStatusEnum.USER_USER_NAME_CONFLICTING;
					}
				}
			}

			if (accessStatusEnum == AccessStatusEnum.UPDATED) {
				session.beginTransaction();
				user.setPassword(encryptionDecryptionUtility.encrypt(user.getPassword()));
				session.update(user);
				session.getTransaction().commit();
				logger.info("User updated successfully, User Details=" + user);
			}
		} catch (Exception e) {
			logger.error("User Update::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return accessStatusEnum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.UserDAO#listUsers()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUsers() {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			List<User> UserList = session.createQuery("from User").list();
			for (User user : UserList) {
				user.setPassword("");
				logger.info("User List::" + user);
			}
			return UserList;
		} catch (Exception e) {
			logger.error("User List::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.UserDAO#getUserById(int)
	 */
	@Override
	public User getUserById(int id) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			User user = (User) session.load(User.class, new Integer(id));
			user.setPassword("");
			logger.info("User loaded successfully, User details=" + user);
			return user;
		} catch (Exception e) {
			logger.error("User get by id::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.UserDAO#deleteUser(int)
	 */
	@Override
	public void deleteUser(int id) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			session.beginTransaction();
			User user = (User) session.load(User.class, new Integer(id));
			if (user != null) {
				session.delete(user);
			}
			session.getTransaction().commit();
			logger.info("User deleted successfully, User details=" + user);
		} catch (Exception e) {
			logger.error("User delete ::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.dao.UserDAO#getUserByUserNameOrEmailAndPassword(com.azilen.azilenProduct.model.UserLogin)
	 */
	@Override
	public User getUserByUserNameOrEmailAndPassword(UserLogin userLogin) {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			User user = (User) session.createQuery("FROM User u where ( u.email = '" + userLogin.getUserNameOrEmail() + "' OR u.userName = '" + userLogin.getUserNameOrEmail() + "') AND u.password = '"
					+ encryptionDecryptionUtility.encrypt(userLogin.getPassword()) + "'").uniqueResult();
			logger.info("User loaded successfully to do authentication for login process, User details=" + user);
			return user;
		} catch (Exception e) {
			logger.error("get User By User Name Or Email And Password::" + e);
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return null;
	}
}
