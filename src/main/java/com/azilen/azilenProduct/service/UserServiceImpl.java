package com.azilen.azilenProduct.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.azilen.azilenProduct.dao.UserDAO;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.model.UserLogin;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class UserServiceImpl implements user service interface.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 14, 2015 12:50:53 PM
 *
 */
@Service
public class UserServiceImpl implements UserService {

	/** The user dao. */
	UserDAO userDAO;

	/**
	 * Sets the user dao.
	 *
	 * @param userDAO
	 *            the new user dao
	 */
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.UserService#saveUser(com.azilen.azilenProduct.model.User)
	 */
	@Override
	public AccessStatusEnum saveUser(User user) {
		return this.userDAO.saveUser(user);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.UserService#updateUser(com.azilen.azilenProduct.model.User)
	 */
	@Override
	public AccessStatusEnum updateUser(User user) {
		return this.userDAO.updateUser(user);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.UserService#listUsers()
	 */
	@Override
	public List<User> listUsers() {
		return this.userDAO.listUsers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.UserService#getUserById(int)
	 */
	@Override
	public User getUserById(int id) {
		return this.userDAO.getUserById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.UserService#deleteUser(int)
	 */
	@Override
	public void deleteUser(int id) {
		this.userDAO.deleteUser(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.UserService#getUserByUserNameOrEmailAndPassword(com.azilen.azilenProduct.model.UserLogin)
	 */
	@Override
	public User getUserByUserNameOrEmailAndPassword(UserLogin userLogin) {
		return this.userDAO.getUserByUserNameOrEmailAndPassword(userLogin);
	}

}
