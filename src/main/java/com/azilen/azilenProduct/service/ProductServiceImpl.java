package com.azilen.azilenProduct.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.azilen.azilenProduct.dao.ProductDAO;
import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.Product;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class ProductServiceImpl implements product service interface .
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 14, 2015 12:50:53 PM
 *
 */
@Service
public class ProductServiceImpl implements ProductService {

	/** The product dao. */
	ProductDAO productDAO;

	/**
	 * Sets the product dao.
	 *
	 * @param productDAO
	 *            the new product dao
	 */
	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.ProductService#saveProduct(com.azilen.azilenProduct.model.Product)
	 */
	@Override
	public AccessStatusEnum saveProduct(Product product) {
		return this.productDAO.saveProduct(product);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.ProductService#updateProduct(com.azilen.azilenProduct.model.Product)
	 */
	@Override
	public AccessStatusEnum updateProduct(Product product) {
		return this.productDAO.updateProduct(product);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.ProductService#listProducts(com.azilen.azilenProduct.model.User)
	 */
	@Override
	public List<Product> listProducts(User loggedInUser) {
		return this.productDAO.listProducts(loggedInUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.ProductService#getProductById(int)
	 */
	@Override
	public Product getProductById(int id) {
		return this.productDAO.getProductById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.ProductService#deleteProduct(int, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public AccessStatusEnum deleteProduct(int id, User loggedInUser) {
		return this.productDAO.deleteProduct(id, loggedInUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.ProductService#searchProductsUsingKeywords(java.lang.String, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public List<Product> searchProductsUsingKeywords(String keywords, User loggedInUser) {
		return this.productDAO.searchProductsUsingKeywords(keywords, loggedInUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.ProductService#listProductsForSelectedCompany(com.azilen.azilenProduct.model.Company, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public List<Product> listProductsForSelectedCompany(Company company, User loggedInUser) {
		return this.productDAO.listProductsForSelectedCompany(company, loggedInUser);
	}

	@Override
	public Product getProductByProductCode(String productCode) {
		return this.productDAO.getProductByProductCode(productCode);
	}
}
