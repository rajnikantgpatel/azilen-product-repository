package com.azilen.azilenProduct.service;

import java.util.List;

import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.model.UserLogin;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserService to serve the user controller action needs.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 14, 2015 12:42:35 PM
 */
public interface UserService {

	/**
	 * Save user.
	 *
	 * @param user the user
	 * @return the access status enum
	 */
	public AccessStatusEnum saveUser(User user);

	/**
	 * Update user.
	 *
	 * @param user the user
	 * @return the access status enum
	 */
	public AccessStatusEnum updateUser(User user);

	/**
	 * List users.
	 *
	 * @return the list
	 */
	public List<User> listUsers();

	/**
	 * Gets the user by id.
	 *
	 * @param id
	 *            the id
	 * @return the user by id
	 */
	public User getUserById(int id);

	/**
	 * Delete user.
	 *
	 * @param id
	 *            the id
	 */
	public void deleteUser(int id);

	/**
	 * Gets the user by user name or email and password.
	 *
	 * @param userLogin
	 *            the user login
	 * @return the user by user name or email and password
	 */
	public User getUserByUserNameOrEmailAndPassword(UserLogin userLogin);
}
