package com.azilen.azilenProduct.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.azilen.azilenProduct.dao.CompanyDAO;
import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.utils.AccessStatusEnum;

// TODO: Auto-generated Javadoc
/**
 * The Class CompanyServiceImpl implements the company service interface.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 14, 2015 12:50:53 PM
 *
 */
@Service
public class CompanyServiceImpl implements CompanyService {

	/** The company dao. */
	CompanyDAO companyDAO;

	/**
	 * Sets the company dao.
	 *
	 * @param companyDAO
	 *            the new company dao
	 */
	public void setCompanyDAO(CompanyDAO companyDAO) {
		this.companyDAO = companyDAO;
	}

	/* (non-Javadoc)
	 * @see com.azilen.azilenProduct.service.CompanyService#saveCompany(com.azilen.azilenProduct.model.Company)
	 */
	@Override
	public AccessStatusEnum saveCompany(Company company) {
		return this.companyDAO.saveCompany(company);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.CompanyService#updateCompany(com.azilen.azilenProduct.model.Company)
	 */
	@Override
	public AccessStatusEnum updateCompany(Company company) {
		return this.companyDAO.updateCompany(company);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.CompanyService#listCompanies(com.azilen.azilenProduct.model.User)
	 */
	@Override
	public List<Company> listCompanies(User loggedInUser) {
		return this.companyDAO.listCompanies(loggedInUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.CompanyService#getCompanyById(int)
	 */
	@Override
	public Company getCompanyById(int id) {
		return this.companyDAO.getCompanyById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.CompanyService#deleteCompany(int, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public AccessStatusEnum deleteCompany(int id, User loggedInUser) {
		return this.companyDAO.deleteCompany(id, loggedInUser);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.azilen.azilenProduct.service.CompanyService#searchCompaniesUsingKeywords(java.lang.String, com.azilen.azilenProduct.model.User)
	 */
	@Override
	public List<Company> searchCompaniesUsingKeywords(String keywords, User loggedInUser) {
		return this.companyDAO.searchCompaniesUsingKeywords(keywords, loggedInUser);
	}

	/* (non-Javadoc)
	 * @see com.azilen.azilenProduct.service.CompanyService#getCompanyByContactEmailAddress(java.lang.String)
	 */
	@Override
	public Company getCompanyByContactEmailAddress(String contactEmailAddress) {
		return this.companyDAO.getCompanyByContactEmailAddress(contactEmailAddress);
	}

}
