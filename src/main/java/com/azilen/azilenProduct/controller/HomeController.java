package com.azilen.azilenProduct.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.azilen.azilenProduct.dao.CompanyDAOImpl;
import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.Product;
import com.azilen.azilenProduct.model.SearchKeyword;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.model.UserLogin;
import com.azilen.azilenProduct.service.CompanyService;
import com.azilen.azilenProduct.service.ProductService;
import com.azilen.azilenProduct.utils.ResourceNotFoundException;

// TODO: Auto-generated Javadoc
/**
 * The Class HomeController to handle welcome like requests.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:25:41 AM
 *
 */
@Controller
public class HomeController {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(CompanyDAOImpl.class);

	/** The company service. */
	private CompanyService companyService;

	/**
	 * Sets the company service.
	 *
	 * @param companyService
	 *            the new company service
	 */
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	/** The product service. */
	private ProductService productService;

	/**
	 * Sets the product service.
	 *
	 * @param productService
	 *            the new product service
	 */
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	/**
	 * Home.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("welcome page called");
		model.addAttribute("searchKeyword", new SearchKeyword());

		if (request.getAttribute("notLoggedIn") != null && !request.getAttribute("notLoggedIn").equals("")) {
			model.addAttribute("errorMessage", request.getAttribute("notLoggedIn"));
		}
		return "home";
	}

	/**
	 * Login.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		logger.info("login page called");
		model.addAttribute("userLogin", new UserLogin());
		model.addAttribute("user", new User());

		return "login";
	}

	/**
	 * About us.
	 *
	 * @return the string
	 */
	@RequestMapping(value = "/aboutUs")
	public String aboutUs() {
		logger.info("about us called");
		return "aboutUs";
	}

	/**
	 * Search company or product.
	 *
	 * @param request
	 *            the request
	 * @param searchKeyword
	 *            the search keyword
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/searchCompanyOrProduct")
	public String searchCompanyOrProduct(HttpServletRequest request, @ModelAttribute SearchKeyword searchKeyword, Model model) {
		String searchCompanyOrProductKeywords = searchKeyword.getSearchCompanyOrProductKeywords();
		if (searchCompanyOrProductKeywords != null && !searchCompanyOrProductKeywords.equals("")) {
			String formattedSearchKeywordsString = searchKeyword.getSeperatedKeywordString();
			HttpSession session = request.getSession();
			User loggedInUser = (User) session.getAttribute("loggedInUser");
			List<Company> listCompanies = this.companyService.searchCompaniesUsingKeywords(formattedSearchKeywordsString, loggedInUser);
			List<Product> listProducts = this.productService.searchProductsUsingKeywords(formattedSearchKeywordsString, loggedInUser);
			model.addAttribute("listCompanies", listCompanies);
			model.addAttribute("listProducts", listProducts);
			model.addAttribute("searchKeyword", searchKeyword);
		} else {
			model.addAttribute("searchKeyword", new SearchKeyword());
			model.addAttribute("errorMessage", "Please enter atleast one keyword.");
		}
		return "home";
	}

	/**
	 * Handle resource not found exception.
	 *
	 * @return the string
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public String handleResourceNotFoundException() {
		return "error";
	}

	/**
	 * Handle blank request.
	 *
	 * @return the string
	 */
	/*@RequestMapping(consumes="text/css", value={"**"})
	public String handleBlankRequest() {
		throw new ResourceNotFoundException();
	}*/
}
