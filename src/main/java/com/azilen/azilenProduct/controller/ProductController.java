package com.azilen.azilenProduct.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.azilen.azilenProduct.dao.CompanyDAOImpl;
import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.Product;
import com.azilen.azilenProduct.model.SearchKeyword;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.service.CompanyService;
import com.azilen.azilenProduct.service.ProductService;
import com.azilen.azilenProduct.utils.AccessStatusEnum;
import com.azilen.azilenProduct.utils.ResourceNotFoundException;

// TODO: Auto-generated Javadoc
/**
 * The Class ProductController to serve the request for Product model.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:26:58 AM
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(CompanyDAOImpl.class);

	/** The product service. */
	private ProductService productService;

	/**
	 * Sets the product service.
	 *
	 * @param productService
	 *            the new product service
	 */
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	/** The company service. */
	private CompanyService companyService;

	/**
	 * Sets the company service.
	 *
	 * @param companyService
	 *            the new company service
	 */
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	/**
	 * List products.
	 *
	 * @param request
	 *            the request
	 * @param companyId
	 *            the company id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public String listProducts(HttpServletRequest request, @RequestParam(required = false, name = "company") String companyId, Model model) {
		logger.info("product list page is called");
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");

		if (companyId != null) {
			Company company = this.companyService.getCompanyById(Integer.parseInt(companyId));
			if (company != null) {
				if (company.getUser().getId() != loggedInUser.getId())
					model.addAttribute("errorMessage", "You don't have access to the selected company.");
				else {
					model.addAttribute("listProducts", this.productService.listProductsForSelectedCompany(company, loggedInUser));
				}
			}
			model.addAttribute("companyId", Integer.parseInt(companyId));
		}

		List<Company> listCompanies = this.companyService.listCompanies(loggedInUser);
		model.addAttribute("listCompanies", listCompanies);
		return "listProducts";
	}

	/**
	 * Adds the product.
	 *
	 * @param request
	 *            the request
	 * @param product
	 *            the product
	 * @param companyId
	 *            the company id
	 * @param model
	 *            the model
	 * @return the string
	 */
	// For add and update product both
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String addProduct(HttpServletRequest request, @ModelAttribute Product product, @RequestParam String companyId, Model model) {

		HttpSession session = request.getSession();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		MultipartFile productImage = product.getFile();
		if (companyId != null && !companyId.equals("0")) {
			logger.info("id : " + product.getId() + " productName : " + product.getProductName() + " productCode : " + product.getProductCode() + " productFeature : " + product.getFeature()
					+ " image : " + product.getImage() + " companyId : " + companyId);
			Company company = this.companyService.getCompanyById(Integer.parseInt(companyId));
			if (loggedInUser.getId() != company.getUser().getId()) {
				model.addAttribute("errorMessage", "You don't have access to the select the selected company to " + ((product.getId() == 0) ? " add" : " update") + " product.");
				model.addAttribute("searchKeyword", new SearchKeyword());
				return "home";
			} else {
				if (productImage != null && productImage.getName() != null && !productImage.equals("")) {
					product.setCompany(company);
					if (product.getId() == 0) {
						// new product, add it
						AccessStatusEnum accessStatusEnum = this.productService.saveProduct(product);
						if (accessStatusEnum == AccessStatusEnum.SAVED) {
							model.addAttribute("successMessage", "Product was added successfully.");
							product = this.productService.getProductByProductCode(product.getProductCode());
							try {
								inputStream = productImage.getInputStream();
								String webappRoot = request.getServletContext().getRealPath("/");
								String relativeFolder = File.separator + "resources" + File.separator + "product" + File.separator;
								String filename = webappRoot + relativeFolder + product.getId() + ".jpg";
								File newFile = new File(filename);
								if (!newFile.exists()) {
									newFile.createNewFile();
								}
								outputStream = new FileOutputStream(newFile);
								int read = 0;
								byte[] bytes = new byte[1024];

								while ((read = inputStream.read(bytes)) != -1) {
									outputStream.write(bytes, 0, read);
								}
								inputStream.close();
								outputStream.close();
							} catch (IllegalStateException | IOException e) {
								logger.error(">-------------------------------->>>>>>   " + e.getMessage());
							}

						} else if (accessStatusEnum == AccessStatusEnum.PRODUCT_PRODUCT_CODE_CONFLICTING) {
							model.addAttribute("errorMessage", "Product code must be unique.");
							model.addAttribute("action", "Add new");
							model.addAttribute("product", product);
							List<Company> listCompanies = this.companyService.listCompanies(loggedInUser);
							model.addAttribute("listCompanies", listCompanies);
							return "addProduct";
						}
					} else {
						// existing product, call update
						AccessStatusEnum accessStatusEnum = this.productService.updateProduct(product);
						if (accessStatusEnum == AccessStatusEnum.UPDATED)
							model.addAttribute("successMessage", "Product was updated successfully.");
						else if (accessStatusEnum == AccessStatusEnum.PRODUCT_PRODUCT_CODE_CONFLICTING) {
							model.addAttribute("errorMessage", "Product code must be unique.");
							model.addAttribute("action", "Edit existing");
							model.addAttribute("product", product);
							List<Company> listCompanies = this.companyService.listCompanies(loggedInUser);
							model.addAttribute("listCompanies", listCompanies);
							return "editProduct";
						}
					}
				} else {
					model.addAttribute("errorMessage", "Please select product image.");
					model.addAttribute("action", "Add new");
					model.addAttribute("product", product);
					return "addProduct";
				}

			}
			return "redirect:/product/products";
		} else {
			model.addAttribute("errorMessage", "Please select a valid company.");
			List<Company> listCompanies = this.companyService.listCompanies(loggedInUser);
			model.addAttribute("listCompanies", listCompanies);
			model.addAttribute("product", product);
			return "editProduct";
		}
	}

	/**
	 * Adds the product.
	 *
	 * @param request
	 *            the request
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addProduct(HttpServletRequest request, Model model) {

		Product product = new Product();
		model.addAttribute("product", product);
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		List<Company> listCompanies = this.companyService.listCompanies(loggedInUser);
		model.addAttribute("listCompanies", listCompanies);

		model.addAttribute("action", "Add new");

		return "addProduct";
	}

	/**
	 * Delete product.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/delete/{id}")
	public String deleteProduct(HttpServletRequest request, @PathVariable("id") int id, Model model) {

		logger.info("Product delete action called to delete product with Id: " + id);
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		AccessStatusEnum accessStatus = this.productService.deleteProduct(id, loggedInUser);

		if (accessStatus == AccessStatusEnum.DELETED)
			model.addAttribute("successMessage", "Product was deleted successfully.");
		else if (accessStatus == AccessStatusEnum.NOT_ACCESSIBLE) {
			model.addAttribute("errorMessage", "You don't have access to the selected product.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		}
		return "redirect:/product/products";
	}

	/**
	 * Edits the product.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/edit/{id}")
	public String editProduct(HttpServletRequest request, @PathVariable("id") int id, Model model) {
		model.addAttribute("action", "Edit existing");
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		Product product = this.productService.getProductById(id);
		if (product == null) {
			model.addAttribute("errorMessage", "Selected product doesn't exist.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else if (product.getCompany().getUser().getId() != loggedInUser.getId()) {
			model.addAttribute("errorMessage", "You don't have access to the selected product.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else {
			List<Company> listCompanies = this.companyService.listCompanies(loggedInUser);
			model.addAttribute("listCompanies", listCompanies);
			model.addAttribute("product", product);
			return "editProduct";
		}
	}

	/**
	 * Show product.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/show/{id}")
	public String showProduct(HttpServletRequest request, @PathVariable("id") int id, Model model) {
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		Product product = this.productService.getProductById(id);
		if (product == null) {
			model.addAttribute("errorMessage", "Selected product doesn't exist.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else if (product.getCompany().getUser().getId() != loggedInUser.getId()) {
			model.addAttribute("errorMessage", "You don't have access to the selected product.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else {
			model.addAttribute("product", product);
		}
		return "showProduct";
	}

	/**
	 * Creates the product.
	 *
	 * @param product
	 *            the product
	 * @return the string
	 */
	@RequestMapping(value = "/ajaxSave", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String createProduct(@RequestBody Product product) {
		productService.saveProduct(product);
		return "success";
	}

	/**
	 * Handle resource not found exception.
	 *
	 * @return the string
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public String handleResourceNotFoundException() {
		return "error";
	}

	/**
	 * Handle blank request.
	 *
	 * @return the string
	 */
	@RequestMapping("**")
	public String handleBlankRequest() {
		throw new ResourceNotFoundException();
	}

}
