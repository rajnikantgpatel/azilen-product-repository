package com.azilen.azilenProduct.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.azilen.azilenProduct.model.SearchKeyword;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.model.UserLogin;
import com.azilen.azilenProduct.service.UserService;
import com.azilen.azilenProduct.utils.AccessStatusEnum;
import com.azilen.azilenProduct.utils.ResourceNotFoundException;

// TODO: Auto-generated Javadoc
/**
 * The Class UserController serves the requests for User model.
 *
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:28:35 AM
 */
@Controller
@RequestMapping("/user")
public class UserController {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	/** The user service. */
	private UserService userService;

	/**
	 * Sets the user service.
	 *
	 * @param userService
	 *            the new user service
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * List users.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String listUsers(Model model) {
		model.addAttribute("listUsers", this.userService.listUsers());
		return "listUsers";
	}

	/**
	 * User login.
	 *
	 * @param request
	 *            the request
	 * @param userLogin
	 *            the user login
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String userLogin(HttpServletRequest request, @ModelAttribute("userLogin") UserLogin userLogin, Model model) {

		String returningPage = "";
		HttpSession session = request.getSession();
		if (session.getAttribute("loggedInUser") != null) {
			model.addAttribute("searchKeyword", new SearchKeyword());
			returningPage = "home";
		} else {
			returningPage = "login";
			model.addAttribute("userLogin", userLogin);
			model.addAttribute("user", new User());
			model.addAttribute("errorMessage", "Please enter correct credentials.");
		}
		return returningPage;
	}

	/**
	 * Adds the user.
	 *
	 * @param user
	 *            the user
	 * @param model
	 *            the model
	 * @return the string
	 */
	// For add and update user both
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("user") User user, Model model) {

		logger.info("email : " + user.getEmail() + " firstName : " + user.getFirstName() + " lastName : " + user.getLastName() + " userName : " + user.getUserName());

		if (user.getId() == 0) {
			// new user, add it
			AccessStatusEnum accessStatusEnum = this.userService.saveUser(user);

			if (accessStatusEnum == AccessStatusEnum.SAVED)
				model.addAttribute("successMessage", "User was added successfully.");
			else if (accessStatusEnum == AccessStatusEnum.USER_EMAIL_CONFLICTING) {
				model.addAttribute("errorMessage", "User email must be unique.");
				model.addAttribute("action", "Add new");
				model.addAttribute("user", user);
				return "addUser";
			} else if (accessStatusEnum == AccessStatusEnum.USER_USER_NAME_CONFLICTING) {
				model.addAttribute("errorMessage", "User name must be unique.");
				model.addAttribute("action", "Add new");
				model.addAttribute("user", user);
				return "addUser";
			}
		} else {
			// existing user, call update
			AccessStatusEnum accessStatusEnum = this.userService.updateUser(user);

			if (accessStatusEnum == AccessStatusEnum.UPDATED)
				model.addAttribute("successMessage", "User was updated successfully.");
			else if (accessStatusEnum == AccessStatusEnum.USER_EMAIL_CONFLICTING) {
				model.addAttribute("errorMessage", "User email must be unique.");
				model.addAttribute("action", " Edit existing");
				model.addAttribute("user", user);
				return "editUser";
			} else if (accessStatusEnum == AccessStatusEnum.USER_USER_NAME_CONFLICTING) {
				model.addAttribute("errorMessage", "User name must be unique.");
				model.addAttribute("action", "Edit existing");
				model.addAttribute("user", user);
				return "editUser";
			}

		}

		return "redirect:/user/users";

	}

	/**
	 * User signup.
	 *
	 * @param user
	 *            the user
	 * @param model
	 *            the model
	 * @return the string
	 */
	// For add and update user both
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String userSignup(@ModelAttribute("user") User user, Model model) {

		logger.info("email : " + user.getEmail() + " firstName : " + user.getFirstName() + " lastName : " + user.getLastName() + " userName : " + user.getUserName());
		AccessStatusEnum accessStatusEnum = this.userService.saveUser(user);

		if (accessStatusEnum == AccessStatusEnum.SAVED)
			model.addAttribute("successMessage", "User was added successfully.");
		else if (accessStatusEnum == AccessStatusEnum.USER_EMAIL_CONFLICTING) {
			model.addAttribute("errorMessage", "You are already registered with the specified email.");
			model.addAttribute("userLogin", new UserLogin());
			model.addAttribute("signUpError", true);
			user.setPassword("");
			model.addAttribute("user", user);
			return "login";
		} else if (accessStatusEnum == AccessStatusEnum.USER_USER_NAME_CONFLICTING) {
			model.addAttribute("errorMessage", "The specified username is already in use. Please use another username.");
			user.setPassword("");
			model.addAttribute("userLogin", new UserLogin());
			model.addAttribute("signUpError", true);
			model.addAttribute("user", user);
			return "login";
		}
		model.addAttribute("userLogin", new UserLogin());
		model.addAttribute("user", new User());

		model.addAttribute("successMessage", "Congrats, you were registered successfully!");
		return "login";

	}

	/**
	 * Adds the user.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addUser(Model model) {
		User user = new User();
		user.setId(0);
		model.addAttribute("user", user);

		model.addAttribute("action", "Add new");

		return "addUser";
	}

	/**
	 * Delete user.
	 *
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") int id, Model model) {
		this.userService.deleteUser(id);
		model.addAttribute("successMessage", "User was deleted successfully.");
		return "redirect:/user/users";
	}

	/**
	 * Edits the user.
	 *
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/edit/{id}")
	public String editUser(@PathVariable("id") int id, Model model) {
		User user = this.userService.getUserById(id);
		if (user == null) {
			model.addAttribute("errorMessage", "Selected user doesn't exist.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else {
			model.addAttribute("action", "Edit existing");
			model.addAttribute("user", user);
			return "editUser";
		}
	}

	/**
	 * Show user.
	 *
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/show/{id}")
	public String showUser(@PathVariable("id") int id, Model model) {
		User user = this.userService.getUserById(id);
		if (user == null) {
			model.addAttribute("errorMessage", "Selected user doesn't exist.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else {
			model.addAttribute("user", user);
			return "showUser";
		}
	}

	/**
	 * Logout user.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/logout")
	public String logoutUser(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		session.setAttribute("loggedInUser", null);
		session.invalidate();
		model.addAttribute("errorMessage", "Logged out successfully.");
		model.addAttribute("searchKeyword", new SearchKeyword());
		return "home";
	}

	/**
	 * Handle resource not found exception.
	 *
	 * @return the string
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public String handleResourceNotFoundException() {
		return "error";
	}

	/**
	 * Handle blank request.
	 *
	 * @return the string
	 */
	@RequestMapping("**")
	public String handleBlankRequest() {
		throw new ResourceNotFoundException();
	}
}
