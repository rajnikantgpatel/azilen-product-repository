package com.azilen.azilenProduct.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.azilen.azilenProduct.model.Company;
import com.azilen.azilenProduct.model.Product;
import com.azilen.azilenProduct.model.SearchKeyword;
import com.azilen.azilenProduct.model.User;
import com.azilen.azilenProduct.service.CompanyService;
import com.azilen.azilenProduct.utils.AccessStatusEnum;
import com.azilen.azilenProduct.utils.ResourceNotFoundException;

// TODO: Auto-generated Javadoc
/**
 * The Class CompanyController is used to handle requests for the model Company.
 * 
 * @author Rajnikant Patel
 * @createdDate Sep 24, 2015 11:24:24 AM
 *
 */
@Controller
@RequestMapping("/company")
public class CompanyController {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(CompanyController.class);

	/** The company service. */
	private CompanyService companyService;

	/**
	 * Sets the company service.
	 *
	 * @param companyService
	 *            the new company service
	 */
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	/**
	 * List companies.
	 *
	 * @param request
	 *            the request
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/companies", method = RequestMethod.GET)
	public String listCompanies(HttpServletRequest request, Model model) {
		logger.info("listCompanies page called");
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		model.addAttribute("listCompanies", this.companyService.listCompanies(loggedInUser));
		return "listCompanies";
	}

	/**
	 * Save company.
	 *
	 * @param request
	 *            the request
	 * @param company
	 *            the company
	 * @param model
	 *            the model
	 * @return the string
	 */
	// For add and update company both
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveCompany(HttpServletRequest request, @ModelAttribute Company company, Model model) {

		HttpSession session = request.getSession();
		InputStream inputStream = null;
		OutputStream outputStream = null;
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		logger.info("Company save page called : with values :::: \nid : " + company.getId() + " companyName : " + company.getCompanyName() + " address : " + company.getAddress() + " logo : "
				+ company.getLogo() + " contactEmailAddress : " + company.getContactEmailAddress() + " employeeStrength : " + company.getEmployeeStrength() + " contactLandlineNumber : "
				+ company.getContactLandlineNumber() + " user : " + loggedInUser.getEmail());
		company.setUser(loggedInUser);

		MultipartFile companyLogo = company.getFile();

		if (companyLogo != null && companyLogo.getName() != null && !companyLogo.equals("")) {
			System.out.println(companyLogo.toString());
			System.out.println(companyLogo.getName());
			if (company.getId() == 0) {
				// new company, add it
				AccessStatusEnum accessStatusEnum = this.companyService.saveCompany(company);
				if (accessStatusEnum == AccessStatusEnum.SAVED) {
					model.addAttribute("successMessage", "Company was added successfully.");
					company = this.companyService.getCompanyByContactEmailAddress(company.getContactEmailAddress());
					try {
						inputStream = companyLogo.getInputStream();
						String webappRoot = request.getServletContext().getRealPath("/");
						String relativeFolder = File.separator + "resources" + File.separator + "company" + File.separator;
						String filename = webappRoot + relativeFolder + company.getId() + ".jpg";
						File newFile = new File(filename);
						if (!newFile.exists()) {
							newFile.createNewFile();
						}
						outputStream = new FileOutputStream(newFile);
						int read = 0;
						byte[] bytes = new byte[1024];

						while ((read = inputStream.read(bytes)) != -1) {
							outputStream.write(bytes, 0, read);
						}
						inputStream.close();
						outputStream.close();
					} catch (IllegalStateException | IOException e) {
						logger.error(">-------------------------------->>>>>> " + e.getMessage());
					}

					finally {
					}
				} else if (accessStatusEnum == AccessStatusEnum.COMPANY_COMPANY_NAME_CONFLICTING) {
					model.addAttribute("errorMessage", "Company name must be unique.");
					model.addAttribute("action", "Add new");
					model.addAttribute("company", company);
					return "addCompany";
				} else if (accessStatusEnum == AccessStatusEnum.COMPANY_CONTACT_EMAIL_ADDRESS_CONFLICTING) {
					model.addAttribute("errorMessage", "Contact email address must be unique.");
					model.addAttribute("action", "Add new");
					model.addAttribute("company", company);
					return "addCompany";
				} else if (accessStatusEnum == AccessStatusEnum.COMPANY_CONTACT_LANDLINE_NUMBER_CONFLICTING) {
					model.addAttribute("errorMessage", "Contact landline number must be unique.");
					model.addAttribute("action", "Add new");
					model.addAttribute("company", company);
					return "addCompany";
				}
			} else {
				AccessStatusEnum accessStatusEnum = this.companyService.updateCompany(company);

				if (accessStatusEnum == AccessStatusEnum.UPDATED) {
					model.addAttribute("successMessage", "Company was updated successfully.");
					try {
						inputStream = companyLogo.getInputStream();
						String webappRoot = request.getServletContext().getRealPath("/");
						String relativeFolder = File.separator + "resources" + File.separator + "company" + File.separator;
						String filename = webappRoot + relativeFolder + company.getId() + ".jpg";
						File newFile = new File(filename);
						if (!newFile.exists()) {
							newFile.createNewFile();
						}
						outputStream = new FileOutputStream(newFile);
						int read = 0;
						byte[] bytes = new byte[1024];

						while ((read = inputStream.read(bytes)) != -1) {
							outputStream.write(bytes, 0, read);
						}
						inputStream.close();
						outputStream.close();
					} catch (IllegalStateException | IOException e) {
						logger.error(">-------------------------------->>>>>>   " + e.getMessage());
					}
				} else if (accessStatusEnum == AccessStatusEnum.NOT_ACCESSIBLE) {
					model.addAttribute("errorMessage", "You don't have access to the selected company.");
					model.addAttribute("searchKeyword", new SearchKeyword());
					return "home";
				} else if (accessStatusEnum == AccessStatusEnum.COMPANY_COMPANY_NAME_CONFLICTING) {
					model.addAttribute("errorMessage", "Company name must be unique.");
					model.addAttribute("action", "Edit existing");
					model.addAttribute("company", company);
					return "editCompany";
				} else if (accessStatusEnum == AccessStatusEnum.COMPANY_CONTACT_EMAIL_ADDRESS_CONFLICTING) {
					model.addAttribute("errorMessage", "Contact email address must be unique.");
					model.addAttribute("action", "Edit existing");
					model.addAttribute("company", company);
					return "editCompany";
				} else if (accessStatusEnum == AccessStatusEnum.COMPANY_CONTACT_LANDLINE_NUMBER_CONFLICTING) {
					model.addAttribute("errorMessage", "Contact landline number must be unique.");
					model.addAttribute("action", "Edit existing");
					model.addAttribute("company", company);
					return "editCompany";
				}
			}
		} else {
			model.addAttribute("errorMessage", "Please select company logo.");
			model.addAttribute("action", "Add new");
			model.addAttribute("company", company);
			return "addCompany";
		}
		return "redirect:/company/companies";
	}

	/**
	 * Adds the company.
	 *
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addCompany(Model model) {
		logger.info("Company add page called.");
		Company company = new Company();
		company.setId(0);
		model.addAttribute("company", company);
		model.addAttribute("action", "Add new");

		return "addCompany";
	}

	/**
	 * Delete company.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/delete/{id}")
	public String deleteCompany(HttpServletRequest request, @PathVariable("id") int id, Model model) {

		logger.info("Company delete action called to delete company with Id: " + id);
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		AccessStatusEnum accessStatus = this.companyService.deleteCompany(id, loggedInUser);

		if (accessStatus == AccessStatusEnum.DELETED)
			model.addAttribute("successMessage", "Company was deleted successfully.");
		else if (accessStatus == AccessStatusEnum.NOT_ACCESSIBLE) {
			model.addAttribute("errorMessage", "You don't have access to the selected company.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else
			model.addAttribute("errorMessage", "Company wasn't deleted as it has some associated products with it.");
		return "redirect:/company/companies";
	}

	/**
	 * Edits the company.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/edit/{id}")
	public String editCompany(HttpServletRequest request, @PathVariable("id") int id, Model model) {
		logger.info("Company edit page called to edit company with id :" + id);
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");

		Company company = this.companyService.getCompanyById(id);
		if (company == null) {
			model.addAttribute("errorMessage", "Selected company doesn't exist.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else if (company.getUser().getId() != loggedInUser.getId()) {
			model.addAttribute("errorMessage", "You don't have access to the selected company.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else {
			model.addAttribute("action", "Edit existing");
			model.addAttribute("company", company);
			return "editCompany";
		}
	}

	/**
	 * Show company.
	 *
	 * @param request
	 *            the request
	 * @param id
	 *            the id
	 * @param model
	 *            the model
	 * @return the string
	 */
	@RequestMapping("/show/{id}")
	public String showCompany(HttpServletRequest request, @PathVariable("id") int id, Model model) {
		logger.info("Company show page called to see details of company with id :" + id);
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");

		Company company = this.companyService.getCompanyById(id);
		if (company == null) {
			model.addAttribute("errorMessage", "Selected company doesn't exist.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else if (company.getUser().getId() != loggedInUser.getId()) {
			model.addAttribute("errorMessage", "You don't have access to the selected company.");
			model.addAttribute("searchKeyword", new SearchKeyword());
			return "home";
		} else {
			model.addAttribute("product", new Product());
			model.addAttribute("company", company);
			return "showCompany";
		}
	}

	/**
	 * Handle resource not found exception.
	 *
	 * @return the string
	 */
	@ExceptionHandler(ResourceNotFoundException.class)
	public String handleResourceNotFoundException() {
		return "error";
	}

	/**
	 * Handle blank request.
	 *
	 * @return the string
	 */
	@RequestMapping(value = { "**" })
	public String handleBlankRequest() {
		throw new ResourceNotFoundException();
	}
}
